//
//  Biblio_generateur_alea.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 26/05/2021.
//

#include "Biblio_generateur_alea.hpp"
#include "tools.hpp"

using namespace std;

//// DiffHB: added C++ STL random generators as global variables
#include <random>
extern mt19937_64 rng;
extern uniform_real_distribution<double> rand_ureal_01;//real Uniform in [0,1)
extern normal_distribution<double> rand_norm;//Strandard Normal(0,1) distribution

//// DiffHB: used C++ random number generators
// double unif(){

//     return(rand()/(RAND_MAX+1.0));
// }


void polaire(vector<double>& tab){//rajoute 2 loi normales O 1 a un vecteur
    //je ne vide pas mon vecteur pour facilite de prog et economie de calculs de n_polaire
    // double theta=unif()*2*PI;
    // double r=sqrt(-2*log(unif()));
    // tab.push_back(r*cos(theta));
    // tab.push_back(r*sin(theta));
    tab.push_back(rand_norm(rng));
    tab.push_back(rand_norm(rng));
}
void polaire(queue<double>& tab){//rajoute 2 loi normales O 1 a une fifo
    //je ne vide pas mon vecteur pour facilite de prog et economie de calculs de n_polaire
    // double theta=unif()*2*PI;
    // double r=sqrt(-2*log(unif()));
    // tab.push(r*cos(theta));
    // tab.push(r*sin(theta));
    tab.push(rand_norm(rng));
    tab.push(rand_norm(rng));
}

void n_polaire(vector<double>& tab, long n,double mu, double sigma){
    tab.clear();
    long m=n%2;
    polaire(tab);
    for(int i(1);i<float(n)/2;i++){//j'ajoute mes loi normales 2 par 2
        polaire(tab);
        tab[2*(i-1)]=tab[2*(i-1)]*sigma+mu;
        tab[2*(i-1)+1]=tab[2*(i-1)+1]*sigma+mu;
    }
    if(m==1){
        tab.pop_back(); // si n impair je retire la derniere noramle
    }
}
void n_polaire(vector<double>& tab, long n){
    tab.clear();
    long m=n%2;
    polaire(tab);
    for(int i(1);i<float(n)/2;i++){//j'ajoute mes loi normales 2 par 2
        polaire(tab);
        tab[2*(i-1)]=tab[2*(i-1)];
        tab[2*(i-1)+1]=tab[2*(i-1)+1];
    }
    if(m==1){
        tab.pop_back(); // si n impair je retire la derniere noramle
    }
}

void n_polaire(queue<double>& tab, long n){
    
    long m=n%2;
    polaire(tab);
    for(int i(1);i<float(n)/2;i++){//j'ajoute mes loi normales 2 par 2
        polaire(tab);
        
    }
    if(m==1){
        tab.pop(); // si n impair je retire la derniere noramle
    }
}


//void n_heavy_tail(vector<double>& va, double alpha, long n){ //simule n va de densité f(t)=lambdaexp(-lambda*t)si t<x_star et beta*y^(-1-alpha) tq f densité et f'(x_star+)=f'(x_star-) calcul dispo p 08/07/2021
 
//// DiffHB: NOTE: telle qu'elle, cette methode ne fait que vider va
    


    /*double lambda=sqrt(alpha*(1+alpha));
    double beta=pow(x_star,alpha)*exp(-lambda*x_star);
    double y_star=1-exp(-lambda*x_star);
    double c_y=pow(beta/(2+alpha),2+alpha);
    double c_y_2=y_star+beta*x_star/(2*alpha);
    */
    
 //   va.clear();
 //   double u=unif();

    
  //  for(long i(0);i<n;i++){
        
        /*if(u<=y_star){
            va.push_back(-log(1-u)/alpha);
        }
        else{
            va.push_back(c_y*pow(c_y_2-u,-1/(2+alpha)));
        }*/
        
//        u=unif();
//    }
//}


double heavy_tail(double alpha){ //simule n va de densité f(t)=(alpha*epsâlpha)/t^(alpha+1)1_{t>eps}
    //simule va de densité f(u)=alpha/(1+u)^1+alpha
    
    
    //// DiffHB: changed eps to 1e-8 because CTRW spurious when eps > dt (binning)
    double eps=1e-8;
    
    //// DiffHB: used C++ random number generators
    double u=rand_ureal_01(rng);
    //double u=unif();
    
    
    
    return(eps*pow(u,-1.0/alpha));
    //return(pow(1-u,-1.0/alpha)-1);
    
    
    
}

void plus_polaire_3(double tab[3], double sigma[3]){//rajoute 3  loi normales N(O,sigma^2) a  un array (par dimension)
    
    // double theta=unif()*2*PI;
    // double r=sqrt(-2*log(unif()));
    // tab[0]+=sigma[0]*r*cos(theta);
    // tab[1]+=sigma[1]*r*sin(theta);
    // theta=unif()*2*PI;
    // r=sqrt(-2*log(unif()));
    // tab[2]+=sigma[2]*r*sin(theta);

    //// DiffHB: used C++ random number generators
    tab[0]+=sigma[0]*rand_norm(rng);
    tab[1]+=sigma[1]*rand_norm(rng);
    tab[2]+=sigma[2]*rand_norm(rng);
    
}

void plus_polaire_3(double tab[3]){//rempli de 3  loi normales O 1 a un array
    
    // double theta=unif()*2*PI;
    // double r=sqrt(-2*log(unif()));
    // tab[0]+=r*cos(theta);
    // tab[1]+=r*sin(theta);
    // theta=unif()*2*PI;
    // r=sqrt(-2*log(unif()));
    // tab[2]+=r*cos(theta);

    //// DiffHB: used C++ random number generators
    tab[0]+=rand_norm(rng);
    tab[1]+=rand_norm(rng);
    tab[2]+=rand_norm(rng);
    
}


double expo(double lambda){ //renvoie une loi expo de param lambda
    
    //// DiffHB: used C++ random number generators
    //return(-log(1-unif())/lambda);
    return(-log(rand_ureal_01(rng))/lambda);
}

double expo_non_homogene(double t,double alpha){//renvoie le prochain tps de saut d'un poisson non homogène d'intensité lambda(s)=t^(alpha-1) sachant qu'on a sauté au temps t
    //// DiffHB: used C++ random number generators
    //return(pow(log(1-unif())+pow(t,alpha),1/alpha));
    return(pow(log(rand_ureal_01(rng))+pow(t,alpha),1/alpha));
}

void unif_boule(double X[3],double rayon){
    //// DiffHB: used C++ random number generators
    //// DiffHB: change to initiate X in [-rayon;rayon] instead of [0,2*Rayon]
    X[0]=rayon*(2*rand_ureal_01(rng)-1);
    X[1]=rayon*(2*rand_ureal_01(rng)-1);
    X[2]=rayon*(2*rand_ureal_01(rng)-1);
    while(norm(X)>rayon){
        X[0]=rayon*(2*rand_ureal_01(rng)-1);
        X[1]=rayon*(2*rand_ureal_01(rng)-1);
        X[2]=rayon*(2*rand_ureal_01(rng)-1);
    }
}


void unif_ellipsoid(double X[3],double sigma[3],double rayon){
    //// DiffHB: used C++ random number generators
    X[0]=rand_ureal_01(rng)*2;
    X[1]=rand_ureal_01(rng)*2;
    X[2]=rand_ureal_01(rng)*2;
    while(norm(X)>1){
        X[0]=rand_ureal_01(rng)*2;
        X[1]=rand_ureal_01(rng)*2;
        X[2]=rand_ureal_01(rng)*2;
    }
    X[0]/=sigma[0]*rayon;
    X[1]/=sigma[1]*rayon;
    X[2]/=sigma[2]*rayon;
}

double poisson_law(double lambda,double T_max){
    int k=0;
    double S=0;
    
    while(S<T_max){
        k++;
        S+=expo(lambda);
    }
    return(k-1);
}


int unif(vector<double> poids){ //retourne une loi uniforme sur [[0,taille_poids]] de proba p(k)=poids[k]/ somme de poids
    double a=0;
    double s=0;
     //// DiffHB: used C++ random number generators
    double u=rand_ureal_01(rng);
    int k=-1;
    for(int i(0);i<poids.size();i++){
        a+=poids[i];
    }
    while(s<=u){
        k++;
        s+=poids[k]/a;
    }
    return(k);
}
