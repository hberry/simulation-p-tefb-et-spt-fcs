//
//  simu_vector_objet.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 27/07/2021.
//

#include "simu_vector_objet.hpp"


using namespace std;

//// DiffHB: added C++ STL random generators as global variables
#include <random>
extern mt19937_64 rng;
extern uniform_real_distribution<double> rand_ureal_01;//real Uniform in [0,1)
extern normal_distribution<double> rand_norm;//Strandard Normal(0,1) distribution


// Simulate a brownian motien in the space
//In parameter : -The diffusion coefficient D (array de dimension 3)
            //-Initial condition of type particule
            //-The iteration number (la longueur finale sera de iter+1)
            //-The step
//The result is a vtrajectoire
vtrajectoire ov_sim_free_mb_d(double D[dimension],particule x0,long iter, double pas){
    
    vtrajectoire traj(D,x0);//initialisation of the trajectoire
    particule construction=x0;//initialisation of my building particule
    int iterator=0;
    
    vector<double> N={};//gaussian simulations
    n_polaire(N, iter*dimension);
    
    
    
    for(int i(1);i<=iter;i++){
        construction.time+=pas;//time iteration
        //// DiffHB: added prefactor 2 to match the definition of D
        for(int k=0;k<dimension;k++){
            construction.space[k]+=sqrt(2*traj.D[k]*pas)*N[iterator];//Euler scheme for every dimension
            iterator++;
            }
        
        traj.observations.push_back(construction);
    }
    N={};
    
    return(traj);
    
}




// Simulate a CTRW in the space
//In parameter : -The MSD's power is alpha
            //-The observation's time of the ctrw (vector in croissant order)
            //-Initial condition as a particule
//The result is a vtrajectoire
vtrajectoire ov_sim_free_ctrw_puis(double alpha, double sigma[dimension], vector<double> temps_obs, particule x0){
    double T=temps_obs.back();
    double zero[3]={0,0,0};
    double un[3]={1,1,1};
    double space[3]={0,0,0};
    int j=0;
    double prespace[3]={0,0,0};
    double time=0;
    vector<particule> initializer;
    for(int i(0);i<temps_obs.size();i++){
        initializer.push_back(particule(space,temps_obs[i]));
    }


    while(time<T or j<temps_obs.size()){
        if(time>temps_obs[j]){
            equal(initializer[j].space,prespace);
            j++;
        }
        else{
            time+=heavy_tail(alpha);
            equal(prespace,space);
            
            //space[0]+=1/sqrt(3);
            //space[1]+=1/sqrt(3);
            //space[2]+=1/sqrt(3);
            plus_polaire_3(space, sigma);
        }
    }
    
    
    return(vtrajectoire(zero,initializer));
    
}


// Calcule le MSD en population  bien donner des trajectoires de même longueur commençant à x=0 en t=0
//En parametre : -Les donnees simulees ou d experimentation (vector de vtrajectoire)
            //-Le vecteur dans lequel sera stocke le resultat (vector de quantities)
            
//Le résultat est un vector de quantities
void ov_msd_MC(vector<vtrajectoire>& data, vector<quantities>& result){ //initialisez result avec une FIFO vide et donner des fifos avec X0=0
    
    long nombre_simu=data.size();
    long taille_simu=data[0].observations.size();
    double s=0;
   
    
    
    for(int i(0);i<taille_simu;i++){
        s=0;
        
            for(long l=0;l<nombre_simu;l++){
                for(int k=0;k<dimension;k++){//norme euclidienne au carré
                    s=s+carre(data[l].observations[i].space[k]);
                    
                    
                }
                
                
                
            }
        result.push_back(quantities(s/nombre_simu,data[0].observations[i].time));//construction du vector de quantities

    }
}



// Calculate the MSD in trajectory (EATAMSD)  you have to give trajectory of same lenght begining in x=0 at t=0
//In parameter : -The simulate datas or experimental datas (vector of vtrajectoire)
            //-The vector in which the result will be stock (vector de quantities)
            
//The result is a vector of quantities
void ov_msd_traj(vector<vtrajectoire> data, vector<quantities>& result){ // you have to give trajectory of same lenght
    
    long nombre_simu=data.size();
    long taille_simu=data[0].observations.size();
    int nombre_sous_traj=0;
    
    double pas=data[0].observations[1].time-data[0].observations[0].time;
    double s=0;
    double S=0;
    
    result.push_back(quantities(0,0));
    
    for(int time(1);time<taille_simu;time++){
        for(int j(0);j<nombre_simu;j++){
            nombre_sous_traj=0;
            s=0;
            for(int i(0);i<floor(taille_simu/time);i++){
                
                
                if((i+1)*time<taille_simu){
                    
                    for(int k(0);k<dimension;k++){
                        s+=carre(data[j].observations[(i+1)*time].space[k]-data[j].observations[i*time].space[k]);
                    }
                    
                    nombre_sous_traj++;
                }
                
            }
            S+=s/nombre_sous_traj;
        }
        S=S/nombre_simu;
        result.push_back(quantities(S,time*pas));
    }
}


    
// Simulate a fractional brownian motion in the space
//In parameter: -The Hurst's coefficient H (array of dimension 3)
            //-The initial condition as a particule
            //-The number of iteration(the final lenght will be of iter+1)
            //-The step
//The result is a vtrajectoire


vtrajectoire ov_sim_free_mbf(/*vector<vector<double>>& test,*/particule x0,long iter,double pas,  double H,double D[dimension]){
    
    /// DiffHB: added a diffusion coefficient independent from the Hurst expo
    double h[3]={H,H,H};
    vtrajectoire traj(h,x0);//initialisation of my vtrajectoire
    vector<double> N;//gaussian simulations
    n_polaire(N, (iter+1)*dimension,0, 1);
    
    double x[dimension]{0};
    double nul[3]{0};
    double t=0;
    
    
    
    vector<vector<double>>cov (iter,vector<double> (iter,0));//my covariance vector
    
    for(int i=0;i<iter;i++){//initialisation of the cov
        for(int j(0);j<=i;j++){
            cov[i][j]=cov_MBS(pas*(i+1),pas*(j+1),H);
            cov[j][i]=cov[i][j];
        }
    }
    cholesky(cov);
    
    
    for(int i(0);i<iter;i++){//cholesky's decomposition time N(0,1) vector
        t=t+pas;
        equal(x,nul);
        for(int k=0;k<dimension;k++){
            for(int m(0);m<=i;m++){
                x[k]+=cov[i][m]*N[dimension*m+k];
            }
            x[k]=sqrt(2.0*D[k])*x[k];
        }
        traj.observations.push_back(particule(x,t));
    }
    
    N={};
    
    return(traj);
}
      
 

    
    
    
// Make a regression to find the power and the multiplicative constant of a vector of quantities
//In parameter :-The vector in which the curve is saved (vector of quantities)
//Le résultat est un array (alpha,beta)
void ov_regression_alpha(vector<quantities> instant,double alphabeta[2]){
    double alpha=0;
    double bar_x=0;
    double bar_y=0;
    double var=0;
    //I start at the first raw to avoid ln 0 when my process start by 0 in 0
    
    
    
    for(int i(1);i<instant.size();i++){
        
        
        
        
        bar_x=bar_x+log(instant[i].time); //average on x
        bar_y=bar_y+log(instant[i].space); //average on y
        
        
    }
    bar_x=bar_x/(instant.size()-1);
    bar_y=bar_y/(instant.size()-1);
    
    
    
    for(int i(1);i<instant.size();i++){
        alpha=alpha+(log(instant[i].time)-bar_x)*(log(instant[i].space)-bar_y);
        var=var+carre(log(instant[i].time)-bar_x);
        
        
        
    }
    
    alphabeta[0]=alpha/var;
    alphabeta[1]=exp(bar_y-alphabeta[0]*bar_x);
}



// Simulate a brownian motion in a ball of radius a, the edge condition a a reflexion
//In parameter : -The diffudion coefficient D (array de dimension 2)
            //-The initial condition as a particule
            //-The number of iteration (la longueur finale sera de iter+1)
            //-The step
//The result is a vtrajectoire
vtrajectoire ov_sim_boule_mb_d2(double D[dimension],particule x0,long iter, double pas,double rayon){
    
    vtrajectoire traj(D,x0);//initialisation of the vtrajectoire
    particule construction=x0;//initialisation of my building particule
    int iterator=0;
    double X0[2];
    double X1[2];
    
    vector<double> N={};//gaussian simulations
    n_polaire(N, iter*2);
    
    
    
    for(int i(1);i<=iter;i++){
        construction.time+=pas;//time iteration
        for(int k=0;k<2;k++){
            construction.space[k]+=traj.D[k]*sqrt(pas)*N[iterator];//euler scheme for every simulation
            iterator++;
            }
        while(norm(construction.space)>rayon){
            X0[0]=traj.observations[i-1].space[0];
            X0[1]=traj.observations[i-1].space[1];
            
            X1[0]=construction.space[0];
            X1[1]=construction.space[1];
            reflexion_boule_d2(X0,X1,rayon);
            construction.space[0]=X0[0];
            construction.space[1]=X0[1];
            
        }
        traj.observations.push_back(construction);
    }
    N={};
    
    return(traj);
    
}

// Simulate a brownian motion in an ellipsoid of equation x^2/sigma[0]+y^2/sigma[1]+z^2/sigma[2]<=rayon, the edge condition a a reflexion
//In parameter : -The diffudion coefficient D (array de dimension 3)
            //-The initial condition as a particule
            //-The number of iteration (la longueur finale sera de iter+1)
            //-The step
//The result is a vtrajectoire
void reflexion_ellipsoid_d3(vtrajectoire simus,double rayon,double sigma[3]){
    
    int end;
    
    double X0[3];
    double X1[3];
    
    
    long iter=simus.observations.size();
    
    for(int i(0);i<iter;i++){
        if(equation_ellipse(simus.observations[i].space, sigma)>carre(rayon)){
            equal(X0,simus.observations[i-1].space);
            equal(X1,simus.observations[i].space);
            while(equation_ellipse(X1, sigma)>carre(rayon)){//} and equation_ellipse(X0, sigma)>carre(rayon) ){
                reflexion_ellipsoid_d3(X0,X1,sigma,rayon);
                
            }
            recenter_vtrajectoire(simus,X1,i);
        }
    }
}

//Simulate the observation of a microscope on a ball inside a bigger domain where the simulations live
//Attention les données après filtrations ne sont pas de départ x0=0
//On observe sur exp(-z^2)<seuil et x^2+y^2<sigma
//Prepare the data with filter step to selection the right step beetween obs
//Prepare data with simu dans noyau

vector<vtrajectoire> filtre_SPT(vector<vtrajectoire> simus_noyau,double centre_observation[dimension], int longeur_mini_obs, long quantite_max_simu_obs,double sigma[dimension],int nombre_iter_par_obs){
    long nombre_simu_obs=0;
    double x0[3]={0,0,0};
    vtrajectoire constructeur(x0,particule(x0,0));
    int j=0;
    int i=0;
    
    int iter=simus_noyau[0].observations.size();
    int taille_constructeur=0;
    double pas=simus_noyau[0].observations[1].time-simus_noyau[0].observations[0].time;
    double time=0;
    vector<vtrajectoire> simus_obs;
    
    while(i<simus_noyau.size() and nombre_simu_obs<quantite_max_simu_obs){
        j=0;
        while(j<iter){
            constructeur.observations.clear();
            taille_constructeur=0;
            time=0;
            while(equation_ellipse(simus_noyau[i].observations[j].space, sigma)<1 and taille_constructeur<longeur_mini_obs and j<iter ){
                if(j%nombre_iter_par_obs==0){
                    constructeur.observations.push_back(particule(simus_noyau[i].observations[j].space,time));
                    taille_constructeur++;
                    time+=pas;
                }
                j++;
            }
            
            if(taille_constructeur>=longeur_mini_obs){
                simus_obs.push_back(constructeur);
                nombre_simu_obs++;
            }
            while(equation_ellipse(simus_noyau[i].observations[j].space, sigma)>1 and taille_constructeur<longeur_mini_obs and j<iter){
                j++;
            }
            
        }
        
        i++;
    }
    return (simus_obs);
}

//Recentre un vecteur de vtrajectoires (x0=0)
void recenter_vector_vtrajectoire(vector<vtrajectoire> &simus){
    double t0;
    double x0[dimension];
    double zero[3]={0,0,0};
    for(int i(0);i<simus.size();i++){
        t0=simus[i].observations[0].time;
        equal(x0,simus[i].observations[0].space);
        simus[i].observations[0].time=0;
        equal(simus[i].observations[0].space,zero);        
        for(int j(1);j<simus[i].observations.size();j++){
            simus[i].observations[j].time-=t0;
            moins(simus[i].observations[j].space,x0);
        }
    }
}

//Recentre un vecteur de vtrajectoires (x_position=x)
void recenter_vtrajectoire(vtrajectoire &simus,double X[3],int position){
   
    double x0[dimension];
    
    
        
        equal(x0,simus.observations[position].space);


        
        for(int j(position);j<simus.observations.size();j++){
            moins(simus.observations[j].space,x0);
            vplus(simus.observations[j].space,X);
        }
    
}

// Calculate the MSD in trajectory (TAMSD)  you have to give trajectory of same lenght begining in x=0 at t=0
//In parameter : -The simulate datas or experimental datas (vector of vtrajectoire)
            //-The vector in which the result will be stock (vector de quantities)
            
//The result is a vector of quantities
vector<vquantities> ov_tamsd(vector<vtrajectoire> data){ // you have to give trajectory of same lenght
    vector<vquantities> retour;
    vquantities construct_result(0,0);
    long nombre_simu=data.size();
    long taille_simu=data[0].observations.size();
    int nombre_sous_traj=0;
    
    double pas=data[0].observations[1].time-data[0].observations[0].time;
    double s=0;
    double S=0;
    
    
    for(int j(0);j<nombre_simu;j++){
        construct_result.result.clear();
        construct_result.result.push_back(quantities(0,0));
        for(int time(1);time<taille_simu;time++){

            //nombre_sous_traj=0;
            s=0;
            for(int i(0);i<floor(taille_simu/time);i++){
                
                
                if((i+1)*time<taille_simu){
                    
                    for(int k(0);k<dimension;k++){
                        s+=carre(data[j].observations[(i+1)*time].space[k]-data[j].observations[i*time].space[k]);
                    }
                    
                    nombre_sous_traj++;
                }
                
            }
            S+=s/nombre_sous_traj;
            construct_result.result.push_back(quantities(S,time*pas));
        }
        retour.push_back(construct_result);
        
    }
    return(retour);
}


// Simulate a CTRW in the space
//In parameter : -The MSD's power is 1
            //-The observation's time of the ctrw (vector in croissant order)
            //-Initial condition as a particule
//The result is a vtrajectoire
vtrajectoire ov_sim_free_ctrw_expo(double alpha, double sigma[dimension], vector<double> temps_obs, particule x0){
    double T=temps_obs.back();
    double zero[3]={0,0,0};
    double space[3]={0,0,0};
    int j=0;
    double prespace[3]={0,0,0};
    double time=temps_obs[j];
    
    vector<particule> initializer;
    for(int i(0);i<temps_obs.size();i++){
        initializer.push_back(particule(space,temps_obs[i]));
    }
    
    while(time<T or j<temps_obs.size()){
        if(time>temps_obs[j]){
            equal(initializer[j].space,prespace);
            j++;
        }
        else{
            time+=expo(alpha);
            equal(prespace,space);
            
            //space[0]+=1/sqrt(3);
            //space[1]+=1/sqrt(3);
            //space[2]+=1/sqrt(3);
            plus_polaire_3(space, sigma);
        }
    }

    
    return(vtrajectoire(zero,initializer));
    
}

