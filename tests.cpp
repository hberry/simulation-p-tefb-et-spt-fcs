//
//  tests.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 31/05/2021.
//

#include "tests.hpp"
#include "Biblio_generateur_alea.hpp"


using namespace std;

int test_variation_quadratique_traj(){ //renvoie 1 si la fonction est ok 0 sinon
    int valid=0;
    vector<vector<double>>tab;
    vector<vector<double>>traj;
    vector<double>init(1,0);
    tab.clear();
    
    for(int i=0;i<10;i++){
        tab.push_back(init);
        tab[i][0]=double(i*i);
        traj.push_back({double(i),double(i)});
    }
    
    int a=variation_quadratique_traj(traj,tab);//on test que lorsque l'on demande des valeurs qui ne sont pas dans le vecteur on renvoie 0
    
    for(int i=0;i<10;i++){
        tab[i][0]=double(i);
        traj.push_back({double(i),double(i)});
    }
    int b=variation_quadratique_traj(traj,tab);//on teste la variation quadra
    
    if(a==0 and b==1 and tab[9][1]==81 and tab[5][1]==25 and tab[0][1]==0){
        valid=1;
    }
    return(valid);
}
    
int test_n_polaire(){
    long n=100000;
    int valid=0;
    double eps=0.01;
    vector<double>tab;
    n_polaire(tab,n,0,1);
    
    double e=0;
    double v=0;
    double s=0;
        
    for(int i(0);i<n;i++){
            
        v=v+carre(tab[i]);
        e=e+tab[i];
        s=s+carre(tab[i])*tab[i];
    }
    v=v/n;
    s=s/n;
    e=e/n;
    
    if(fabs(v-1)<eps and fabs(e)<eps and fabs(s)<eps and tab.size()==n){
        valid=1;
    }
    
    return(valid);
}
    
int test_msd_MC_d1(){
    int valid=1;
    int n=10000;
    int N=1000;
    int K=100;
    
    vector<vector<double>>test1(1,vector<double>(n+1,0));
    vector<vector<double>>test2(1,vector<double>(n+1,0));
    vector<vector<double>>instants1(1,vector<double>(2,0));
    vector<vector<double>>instants2 (1,vector<double>(2,0));
  
    for(int i=1;i<=N;i++){ //j'initialise mes vecteurs test come une suite de i et de i^2
        test1.push_back(vector<double>(n+1,i));
        test2.push_back(vector<double>(n+1,i*i));
        test2[i][0]=i;
    }
    
    
    for(int i=1;i<=K;i++){
        instants1.push_back(vector<double> (2,i*10));
    }

    instants2=instants1;
    
    int a=msd_MC_d1(test1,instants1);
    int b=msd_MC_d1(test2,instants2);
    
    for(int i=1;i<K;i++){
        if(instants1[i][1]!=carre(test1[i*10][1])or instants2[i][1]!=carre(test2[i*10][1])){
            valid=0;
        }
    }
    

    if(a==0 or b==0){
        valid=0;
    }
    
    return(valid);

}


int test_sim_n_free_mb_d1(){
    double eps=0.01;
    int valid=1;
    double D=1;
    int iter=10;
    double t_min=0;
    double t_max=10;
    double nombre_simu=100000000;
    
    vector<vector<double>>b_0 (1,vector<double>(nombre_simu,0));
    
    for(int i(0);i<nombre_simu;i++){
        b_0[0][i]=0;
    }
    
    sim_n_free_mb_d1(t_min,t_max, b_0, D, iter);
    
    vector<vector<double>> temps (1,vector<double>(2,0));
    for(int i(1);i<=10;i++){
        temps.push_back(vector<double>(2,double(i)));
    }
    
    int a=msd_MC_d1(b_0, temps);
    
    if(a==0){
        valid=0;
    }
    
    for(int i(0);i<=10;i++){
        if(fabs(temps[i][1]-i)>eps){
            valid=0;
        }
    }
    
    return(valid);
}


int test_msd_MC(){
    double dim=3;
    int valid=1;
    int n=10000; //nombre de simu par vecteurs
    int N=1000;//nmbre de pts par traj
    int K=100;//nombre d'instants d'analyse
    
    vector<vector<vector<double>>>test1(1,vector<vector<double>>(n+1,vector<double>(dim,0)));
    vector<vector<vector<double>>>test2(1,vector<vector<double>>(n+1,vector<double>(dim,0)));
    vector<vector<double>>instants1(1,vector<double>(2,0));
    vector<vector<double>>instants2 (1,vector<double>(2,0));
  
    for(int i=1;i<=N;i++){ //j'initialise mes vecteurs test come une suite de i et de i^2
        test1.push_back(vector<vector<double>>(n+1,vector<double>(dim,i)));
        test2.push_back(vector<vector<double>>(n+1,vector<double>(dim,i*i)));
        test2[i][0]=vector<double>(2,i);
    }
    
    
    for(int i=1;i<=K;i++){
        instants1.push_back(vector<double> (2,i*10));
    }

    instants2=instants1;
    
    int a=msd_MC(test1,instants1);
    int b=msd_MC(test2,instants2);
    
    for(int i=1;i<K;i++){
        if(instants1[i][1]!=dim*carre(10*i)or instants2[i][1]!=dim*carre(carre(10*i))){
            valid=0;
        }
    }
    

    if(a==0 or b==0){
        valid=0;
    }
    
    return(valid);

}


int test_sim_compact_mb_d1_reflexion(){
    double eps=0.1;
    int valid=1;
    double D=10000;
    int iter=10;
    double t_min=0;
    double t_max=10;
    double nombre_simu=100000;
    double sup=5;
    double inf=-5;
    
    
    vector<vector<double>>b_0 (1,vector<double>(nombre_simu,0));
    
    for(int i(0);i<nombre_simu;i++){
        b_0[0][i]=0;
    }
    
    
    sim_n_compact_mb_d1_reflexion(t_min,t_max,b_0,D,iter,sup,inf);
    
    vector<vector<double>> temps (1,vector<double>(2,0));
    for(int i(1);i<=10;i++){
        temps.push_back(vector<double>(2,double(i)));
    }
    
    int a=msd_MC_d1(b_0, temps);
    
    for(int i(0);i<temps.size();i++){
        if(temps[i][1]>carre(inf) or temps[i][1]>carre(sup)){
            valid=0;
        }
        
    }
    
    sup=10000;
    inf=-10000;
    D=1;
    
    sim_n_compact_mb_d1_reflexion(t_min,t_max,b_0,D,iter,sup,inf);
    a=msd_MC_d1(b_0, temps);
    
    if(a==0){
        valid=0;
    }
    
    for(int i(0);i<=10;i++){
        if(fabs(temps[i][1]-i)>eps){
            valid=0;
        }
    }
    
    return(valid);
}


int test_sim_free_ctrw_d(){
    double valid=1;
    double alpha=0.5;
    double eps=carre(0.1);
    double mu=0;
    double sigma=1;
    long nombre_simu=100000;
    long dim=1;
    long pas=100;
    vector<vector<vector<double>>> instants(pas+1,vector<vector<double>>(nombre_simu+1,vector<double>(dim,0)));
    vector<vector<double>> instant_obs(pas+1,vector<double>(dim,0));
    
    for(int i(0);i<pas;i++){
        instants[i+1][0]=vector<double>(dim,i+1);
        instant_obs[i+1]=vector<double>(dim,i+1);
    }
    
    sim_free_ctrw_d(alpha, mu,sigma, nombre_simu,instants);
    ;
    int a=msd_MC(instants, instant_obs);
    
    
    
    
    regression_alpha(instant_obs);
    if(carre(instant_obs[0][0]-alpha)>eps){
        valid =0;
    }
    instants=vector<vector<vector<double>>> (pas+1,vector<vector<double>>(nombre_simu+1,vector<double>(dim,0)));
    instant_obs=vector<vector<double>> (pas+1,vector<double>(dim,0));
    
    for(int i(0);i<pas;i++){
        instants[i+1][0]=vector<double>(dim,i+1);
        instant_obs[i+1]=vector<double>(dim,i+1);
    }

    alpha=0.75;
    sim_free_ctrw_d(alpha, mu,sigma, nombre_simu,instants);

    a=msd_MC(instants, instant_obs);




    regression_alpha(instant_obs);
    if(carre(instant_obs[0][0]-alpha)>eps){
        valid =0;
    }
    
    
    instants=vector<vector<vector<double>>> (pas+1,vector<vector<double>>(nombre_simu+1,vector<double>(dim,0)));
    instant_obs=vector<vector<double>> (pas+1,vector<double>(dim,0));
    
    for(int i(0);i<pas;i++){
        instants[i+1][0]=vector<double>(dim,i+1);
        instant_obs[i+1]=vector<double>(dim,i+1);
    }
    alpha=0.25;
    sim_free_ctrw_d(alpha, mu,sigma, nombre_simu,instants);
    ;
    a=msd_MC(instants, instant_obs);




    regression_alpha(instant_obs);
    if(carre(instant_obs[0][0]-alpha)>eps){
        valid =0;
    }
    return(valid);
}
