//
//  MBF.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 18/06/2021.
//

#ifndef MBF_hpp
//#define MBF_hpp

#include "Biblio_generateur_alea.hpp"
#include "tools.hpp"
#include <stdio.h>

void sim_n_free_mbf_d(double t_min, double t_max, vector<vector<vector<double>>>& b_0, vector<double> D, long iter,vector<double> H);

#endif /* MBF_hpp */
