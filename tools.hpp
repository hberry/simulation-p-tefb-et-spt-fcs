//
//  tools.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 31/05/2021.
//

#ifndef tools_hpp
#define tools_hpp

#include "Biblio_generateur_alea.hpp"
#include <stdio.h>
#include "simu_objet.hpp"

//const int dimension=3;


double carre(double a);
int variation_quadratique_traj(vector<vector<double>> donnees, vector<vector<double>>& instants);
int msd_MC_d1(vector<vector<double>> data, vector<vector<double>>& instant);
int msd_MC(vector<vector<vector<double>>> data, vector<vector<double>>& instant);
void reflexion_boule_d2(vector<double>& X0,vector<double>& X1,double  a);
void reflexion_boule_d2(double X0[2],double X1[2],double  a);
double norm(vector<double>X);
double norm(double X[dimension]);

double ps(vector<double>X,vector<double>Y);
void regression_alpha(vector<vector<double>>& instant);
void decomp_mat_tri_inf(vector<vector<double>>A, vector<vector<double>>&L);
void cholesky(vector<vector<double>>& data);

void equal(double x_0[dimension],double x_1[dimension]);
void equal2(double x_0[2],double x_1[2]);
double cov_MBS(double s,double t, double H);
double cov_MBS(double s,double t, double H[dimension]);
double ps(double X[3],double Y[3]);
long factorielle(int valeur);
double distance(double x[dimension],double y[dimension]);
void moins(double x[dimension],double y[dimension]);
void vplus(double x[dimension],double y[dimension]);
double gaussian_intensity(double x[3],double sigma[3]);
void reflexion_ellipsoid_d3(double X0[3],double X1[3],double  sigma[3],double rayon);//fais une réflexion dans ellipse k(x,y,z)=x^2/a+y^2/b+z^2/c  cahier 2 13/09
double equation_ellipse(double X[3],double  sigma[3]);

#endif /* tools_hpp */
