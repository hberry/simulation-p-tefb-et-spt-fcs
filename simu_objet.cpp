//
//  simu_objet.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 18/06/2021.
//

#include "simu_objet.hpp"

using namespace std;


// Simule un mouvement brownien dans l'espace
//En parametre : -Le coef de diffusion D (array de dimension 3)
            //-La condition initiale sous forme d'une particule
            //-Le nombre d'itérations (la longueur finale sera de iter+1
            //-Le pas
//Le résultat est une trajectoire
trajectoire o_sim_free_mb_d(double D[3],particule x0,long iter, double pas){
    
    trajectoire traj(D,x0);//initialisation de la trajectoire
    particule construction=x0;//initialisation de ma particule de construction
    
    queue<double> N={};//les simulations de loi normale
    n_polaire(N, iter*dimension);
    
    
    
    for(int i(1);i<=iter;i++){
        construction.time+=pas;//itération du temps
        for(int k=0;k<dimension;k++){
            construction.space[k]+=traj.D[k]*sqrt(pas)*N.front();//schéma d'euler pour chaques dimensions
            N.pop();
            }
        
        traj.observations.push(construction);
    }
    N={};
    
    return(traj);
    
}


// Simule une CTRW dans l'espace
//En parametre : -La puissance du MSD est alpha
            //-Les temps auxquels on veut observer la ctrw (queue)
            //-La condition initiale sous forme d'une particule
//Le résultat est une trajectoire
trajectoire o_sim_free_ctrw_puis(double alpha, queue<double> temps_obs, particule x0){
    double D[3]={0,0,0};//pas d'utilité actuellement mis à part initialiser la trajectoire
    trajectoire B(D,x0);//la trajectoire à remplir
    
    //vector<double>normal;
    double temps=B.observations.front().time;//initialise t_min
    double x[dimension];
    double x_1[dimension];//variable de mémoire
    
    
    equal(x,B.observations.front().space);//initialise x_0
    //B.observations.pop();
    equal(x_1,x);
    plus_polaire_3(x_1);
   
    
    while(temps_obs.size()>0){
        while(temps<temps_obs.front()){//lorsqu'une loi expo saute après le temps d'observation que je veux on stock la dernière position avant le saut
            temps+=heavy_tail(alpha);
            equal(x,x_1);
            plus_polaire_3(x_1);
            
            

        }
        B.observations.push(particule(x,temps_obs.front()));
        temps_obs.pop();
    }
    temps_obs={};
    return(B);
}



trajectoire o_sim_free_ctrw_pois(double alpha,  queue<double> temps_obs, particule x0/*,trajectoire& B*/){//instants contient aussi x0 en plus des instants d'observation colonne 1 les tps d'obs(vecteur de dimension d) et les autres colonnes les ctrw aux temps d'obs
    double D[3]={0,0,0};
    trajectoire B(D,x0);
    
    vector<double>normal;
    double temps=B.observations.front().time;
    double x[dimension];
    double x_1[dimension];
    
    
    equal(x,B.observations.front().space);
    B.observations.pop();
    equal(x_1,x);
    plus_polaire_3(x_1);
   
    
    while(temps_obs.size()>0){
        while(temps<temps_obs.front()){
            temps+=expo_non_homogene(temps,alpha);
            equal(x,x_1);
            plus_polaire_3(x_1);

        }
        B.observations.push(particule(x,temps_obs.front()));
        temps_obs.pop();
    }
    return(B);
}


void msd_MC(vector<trajectoire>& data, queue<quantities>& result){ //initialisez result avec une FIFO vide et donner des fifos avec X0=0
    
    long nombre_simu=data.size();
    long taille_simu=data.front().observations.size();
    double s=0;
    double t=0;
    
    
    for(int i(0);i<taille_simu;i++){
        s=0;
        t=data[0].observations.front().time;
            for(long l=0;l<nombre_simu;l++){
                for(int k=0;k<dimension;k++){//norme euclidienne au carré
                    s=s+carre(data[l].observations.front().space[k]);
                    
                    
                }
                
                data[l].observations.pop();
                
            }
        result.push(quantities(s/nombre_simu,t));

    }
}


void msd_traj(vector<trajectoire> data, queue<quantities>& result,double pas){ //initialisez result avec une FIFO vide et donner des traj de même taille //renvoie un vecteur 2x2 avec colonne 0 les temps et colonne 1 les msd attention bien donner instant un vecteur 2x2 colonnes 0 les temps colonne 1 peu importe
    
    long nombre_simu=data.size();
    long taille_simu=data.front().observations.size();
    vector<trajectoire> copy;
    double s=0;
    double tampon[dimension];
    
    for(int time(1);time<floor(taille_simu/2);time++){
        copy={};
        copy=data;
        s=0;
        for(int j(0);j<nombre_simu;j++){
            for(int i(0);i<floor(taille_simu/time)-1;i++){
               
                equal(tampon,copy[j].observations.front().space);
                
                for(int k(0);k<time;k++){
                    
                        copy[j].observations.pop();
                    
                }
                
                for(int k(0);k<dimension;k++){
                    s+=carre(copy[j].observations.front().space[k]-tampon[k])/(floor(taille_simu/time)-1);
                }
                
            }
        }
        s=s/nombre_simu;
        result.push(quantities(s,time*pas));
    }
}

trajectoire o_sim_free_mbf(vector<vector<double>>& test,particule x0,long iter, double pas,  double H){  // simule des brownien frac tq B_t_min=b_0(vecteur de vecteur de dim d)  jusqu'à t_max en iter pas. colone 0 les instants colonnes i>0 les valeur du brownien
    
    double h[3]={H,H,H};
    trajectoire traj(h,x0);
    vector<double> N;//nos simulation de loi normale
    n_polaire(N, (iter+1)*dimension,0, 1);
    
    double x[dimension]{0};
    double nul[3]{0};
    double t=0;
   
    vector<vector<double>>cov (iter,vector<double> (iter,0));//je déclare mon vecteur de cov
    vector<vector<double>>tri_inf (iter,vector<double> (iter,0));//je déclare mon vecteur de cov décomposé
    
    for(int i=0;i<iter;i++){//initialisation de matrice de cov
        for(int j(0);j<=i;j++){
            cov[i][j]=cov_MBS(pas*(i+1),pas*(j+1),H);
            cov[j][i]=cov[i][j];
        }
    }
    //decomp_mat_tri_inf(cov , tri_inf);
    //cholesky(cov);
    
    test=cov;
    /*
    traj.observations.push(particule(x,t));
    
    for(int i(1);i<=iter;i++){
        t=t+pas;
        equal(x,nul);
        for(int k=0;k<dimension;k++){
            for(int m(0);m<i;m++){
                x[k]+=tri_inf[i][m]*N[dimension*m+k];
            }
        }
        traj.observations.push(particule(x,t));
    }
    
    N={};
    */
    return(traj);
}
      
    
void regression_alpha(queue<quantities> instant,double alphabeta[2]){ //je commence à la ligne 1 pour éviter d'avoir ln 0 quand mon proc part de 0 en 0
    double alpha=0;
    double bar_x=0;
    double bar_y=0;
    double var=0;
    
    instant.pop();//je commence à la ligne 1 pour éviter d'avoir ln 0 quand mon proc part de 0 en 0
    
    for(int i(0);i<instant.size()-1;i++){
        
        instant.push(instant.front());
        
        
        bar_x=bar_x+log(instant.front().time);
        bar_y=bar_y+log(instant.front().space);
        
        instant.pop();
    }
    bar_x=bar_x/(instant.size()-1);
    bar_y=bar_y/(instant.size()-1);
    
    
    
    for(int i(0);i<instant.size();i++){
        alpha=alpha+(log(instant.front().time)-bar_x)*(log(instant.front().space)-bar_y);
        var=var+carre(log(instant.front().time)-bar_x);
        instant.push(instant.front());
        instant.pop();
        
        
    }
    
    alphabeta[0]=alpha/var;
    alphabeta[1]=exp(bar_y-alphabeta[0]*bar_x);
}


void auto_correlation_de_traj(vector<trajectoire>& data, queue<quantities>& result,double pas){
    queue<quantities> constructeur;
    queue<queue<quantities>> auto_par_traj;
    queue<particule> traj_shift;
    double s;

    
    
    
    for(int N(0);N<data.size();N++){
        for(int tau(1);tau<=data[N].observations.size()-1;tau++){
            traj_shift=data[N].observations;
            for(int k(0);k<tau;k++){
                traj_shift.pop();
            }
            s=0;
            for(int i(0);i<data[N].observations.size()-tau;i++){
                s+=ps(data[N].observations.front().space,traj_shift.front().space);
                traj_shift.pop();
                data[N].observations.push(data[N].observations.front());
                data[N].observations.pop();
                
            }
            for(int k(0);k<tau;k++){
                data[N].observations.push(data[N].observations.front());
                data[N].observations.pop();
            }
            constructeur.push(quantities(s/(data[N].observations.size()-tau),tau*pas));
        }
        auto_par_traj.push(constructeur);
        while(constructeur.size()>0){
            constructeur.pop();
        }
    }
    
    for(int tau(1);tau<=data[0].observations.size()-1;tau++){
        s=0;
        for(int N(0);N<data.size();N++){
            s+=auto_par_traj.front().front().space;
            auto_par_traj.front().pop();
            auto_par_traj.push(auto_par_traj.front());
            auto_par_traj.pop();
        }
        result.push(quantities(s/data.size(),tau*pas));
    }
    
    
}


void auto_correlation(vector<FCS_photons>& data, queue<quantities>& result,double pas){
    queue<quantities> utilitaire;
    queue<queue<quantities>> auto_par_traj;
    queue<quantities> traj_shift;
    double s=0;
    double t;

    
    for(int n(0);n<data[0].observations.size();n++){
        t=data[0].observations.front().time;
        for(int i(0);i<data.size();i++){
            s+=data[i].observations.front().space;
            data[i].observations.push(data[i].observations.front());
            data[i].observations.pop();
            }
        utilitaire.push(quantities(s/data.size(),t));
    }
    
    for(int n(0);n<data[0].observations.size();n++){
        for(int i(0);i<data.size();i++){
            data[i].observations.front().space=data[i].observations.front().space-utilitaire.front().space;
            data[i].observations.push(data[i].observations.front());
            data[i].observations.pop();
            }
        utilitaire.pop();
    }
    utilitaire={};
    for(int n(0);n<data[0].observations.size();n++){
        t=data[0].observations.front().time;
        for(int i(0);i<data.size();i++){
            s+=carre(data[i].observations.front().space);
            data[i].observations.push(data[i].observations.front());
            data[i].observations.pop();
            }
        utilitaire.push(quantities(s/data.size(),t));
    }
    
    
    for(int n(0);n<data[0].observations.size();n++){
        for(int i(0);i<data.size();i++){
            data[i].observations.front().space=data[i].observations.front().space/sqrt(utilitaire.front().space);
            data[i].observations.push(data[i].observations.front());
            data[i].observations.pop();
            }
        utilitaire.pop();
    }
    
    utilitaire={};
    
    
    
    for(int N(0);N<data.size();N++){
        for(int tau(1);tau<=data[N].observations.size()-1;tau++){
            traj_shift=data[N].observations;
            for(int k(0);k<tau;k++){
                traj_shift.pop();
            }
            s=0;
            for(int i(0);i<data[N].observations.size()-tau;i++){
                s+=data[N].observations.front().space*traj_shift.front().space;
                traj_shift.pop();
                data[N].observations.push(data[N].observations.front());
                data[N].observations.pop();
                
            }
            for(int k(0);k<tau;k++){
                data[N].observations.push(data[N].observations.front());
                data[N].observations.pop();
            }
            utilitaire.push(quantities(s/(data[N].observations.size()-tau),tau*pas));
        }
        auto_par_traj.push(utilitaire);
        utilitaire={};
    }
    
    for(int tau(1);tau<=data[0].observations.size()-1;tau++){
        s=0;
        for(int N(0);N<data.size();N++){
            s+=auto_par_traj.front().front().space;
            auto_par_traj.front().pop();
            auto_par_traj.push(auto_par_traj.front());
            auto_par_traj.pop();
        }
        result.push(quantities(s/data.size(),tau*pas));
    }
    auto_par_traj={};
    utilitaire={};
    
}

