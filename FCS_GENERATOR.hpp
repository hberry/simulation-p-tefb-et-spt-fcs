//
//  FCS_GENERATOR.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 05/08/2021.
//

#ifndef FCS_GENERATOR_hpp
#define FCS_GENERATOR_hpp

#include <stdio.h>
#include "Biblio_generateur_alea.hpp"
#include "tests.hpp"
#include "simu_vector_objet.hpp"
#include "tools.hpp"
#include <stdlib.h>
#include<cstdlib>
#include<cmath>


class FCS_train{
public:
    vector<quantities>observations;
    
    double lenght;
    FCS_train( quantities b_0){
       //
        observations.clear();
        observations.push_back(b_0);
    }
    
    FCS_train( vector<quantities>data){
       
        
        observations=data;
    }
    
    ~FCS_train(){
        observations.clear();
        
    }
    
};

void new_start_sphere(vtrajectoire & traj, long range_particule, double radius);//OK
void new_start_ellipsoid(vtrajectoire & traj, long range_particule, double sigma[3]);//ok

double gaussian_intensity_segment_interpolation(vtrajectoire traj, double time, double step, double sigma[3],double intensity);//ok
void trajectory_preparation(vtrajectoire& traj,double radius);//ok
void trajectory_preparation_ellipsoid(vtrajectoire& traj,double sigma[3],double radius);//ok
vector<double> generate_FCS_train(vector<vtrajectoire> trajs, double sigma[3],double radius,double T_max,double illumination,double lambda_poisson_bleach);//seems ok
FCS_train initialise_FCS_empty(long length,double step);//ok
void random_start(vtrajectoire& traj, double radius);//ok
void random_start_ellipsoid(vtrajectoire& traj, double sigma[3],double radius);//ok
vector<FCS_train> generate_normalised_FCS_train_MC(vector<vtrajectoire> simus, double sigma[3],double radius,double T_max,double illumination,double mean_emission_before_bleaching,long nombre_particule_dans_noyau,double timestep,double rayon_noyau);
vector<quantities> autocorrelation_function(vector<FCS_train> simus_normalised);
FCS_train bin_FCS_train(vector<double> arrival,double timestep,double T_max);//ok

#endif /* FCS_GENERATOR_hpp */
