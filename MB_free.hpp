//
//  MB_free.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 26/05/2021.
//

#ifndef MB_free_hpp
//#define MB_free_hpp

#include "Biblio_generateur_alea.hpp"

void sim_free_mb_d1(vector<vector<double>>& traj, double t_min, double t_max, double b_0, double D, int iter);
void sim_n_free_mb_d1(double t_min, double t_max, vector<vector<double>>& b_0, double D, int iter);
void sim_n_free_mb_d(double t_min, double t_max, vector<vector<vector<double>>>& b_0, vector<double> D, long iter);

#include <stdio.h>

#endif /* MB_free_hpp */
