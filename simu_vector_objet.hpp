//
//  simu_vector_objet.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 27/07/2021.
//

#ifndef simu_vector_objet_hpp
#define simu_vector_objet_hpp

#include"Biblio_generateur_alea.hpp"
#include <stdio.h>
#include"tools.hpp"
#include "simu_objet.hpp"


/*const int dimension=3;

class particule{
public:
    
    double time;
    double space[dimension];
    
    particule(double x[dimension],double t){
        
        for(int i(0);i<dimension;i++){
            space[i]=x[i];
        }
        time=t;
        }
    
    
};*/

class vtrajectoire{
public:
    vector<particule>observations;
    
    double D[dimension];
    vtrajectoire( double d[dimension],particule b_0){
        
        for(int i(0);i<dimension;i++){
            D[i]=d[i];
        }
        observations.push_back(b_0);
    }
    
    vtrajectoire( double d[dimension],vector<particule>data){
       
        for(int i(0);i<dimension;i++){
            D[i]=d[i];
        }
        observations=data;
    }
    
    ~vtrajectoire(){
        observations.clear();
        
    }
    
};

class vquantities{
public:
    
    vector<quantities> result;
    
    vquantities(double x,double t){
        
        result.push_back(quantities(x,t));
        
    }
    
    
};


vtrajectoire ov_sim_free_mb_d(double D[dimension],particule x0,long iter, double pas);
vtrajectoire ov_sim_free_ctrw_puis(double alpha,double sigma[dimension], vector<double> temps_obs, particule x0);
vtrajectoire ov_sim_boule_mb_d2(double D[dimension],particule x0,long iter, double pas,double rayon);
void reflexion_ellipsoid_d3(vtrajectoire simus,double rayon,double sigma[3]);
void ov_msd_MC(vector<vtrajectoire>& data, vector<quantities>& result);
void ov_msd_traj(vector<vtrajectoire> data, vector<quantities>& result);

    /// DiffHB: added a diffusion coefficient independent from the Hurst expo
vtrajectoire ov_sim_free_mbf(/*vector<vector<double>>& test,*/particule x0,long iter, double pas,  double H,double D[dimension]);
void ov_regression_alpha(vector<quantities> instant,double alphabeta[2]);
vector<vtrajectoire> filtre_SPT(vector<vtrajectoire> simus_noyau,double centre_observation[dimension], int longeur_mini_obs, long quantite_max_simu_obs,double sigma[dimension],int nombre_iter_par_obs);
void recenter_vector_vtrajectoire(vector<vtrajectoire> &simus);
void recenter_vtrajectoire(vtrajectoire &simus,double X[3],int position);
vector<vquantities> ov_tamsd(vector<vtrajectoire> data);
vtrajectoire ov_sim_free_ctrw_expo(double alpha, double sigma[dimension], vector<double> temps_obs, particule x0);


#endif /* simu_vector_objet_hpp */
