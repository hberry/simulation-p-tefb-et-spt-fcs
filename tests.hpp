//
//  tests.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 31/05/2021.
//

#ifndef tests_hpp
//#define tests_hpp
#include "Biblio_generateur_alea.hpp"
#include "MB_free.hpp"
#include "tools.hpp"
#include "MB_Compact.hpp"
#include "CTRW_free.hpp"
#include "MBF.hpp"
#include "simu_objet.hpp"


int test_variation_quadratique_traj();
int test_n_polaire();
int test_msd_MC_d1();
int test_sim_n_free_mb_d1();
int test_sim_compact_mb_d1_reflexion();
int test_msd_MC();
int test_sim_free_ctrw_d();

#include <stdio.h>

#endif /* tests_hpp */
