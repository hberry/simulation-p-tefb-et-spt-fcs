//
//  MBF.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 18/06/2021.
//

#include "MBF.hpp"
using namespace std;


//attention vérifier le mbf
void sim_n_free_mbf_d(double t_min, double t_max, vector<vector<vector<double>>>& b_0, vector<double> D, long iter,vector<double> H){  // simule des brownien frac tq B_t_min=b_0(vecteur de vecteur de dim d)  jusqu'à t_max en iter pas. colone 0 les instants colonnes i>0 les valeur du brownien
    
    long dimension=b_0[0][0].size();
    double h=(t_max-t_min)/iter;//le pas
    vector<double> N;//nos simulation de loi normale
    long l=b_0[0].size(); //le nombre de simu qu'on veut
    n_polaire(N, iter*l*dimension,0, 1);
    
    vector<vector<double>>tab (l+1,vector<double> (dimension,t_min));//je déclare mon vecteur de construction
    vector<vector<double>>cov (l+1,vector<double> (l+1,0));//je déclare mon vecteur de cov
    vector<vector<double>>tri_inf (l+1,vector<double> (l+1,0));//je déclare mon vecteur de cov décomposé
    for(int i=1;i<=l;i++){ //j'initialise t0 x0
        tab[i]=b_0[0][i-1]; //j'initialise mon vecteur de construction
    }
    for(int i=0;i<=l;i++){//initialisation de matrice de cov
        for(int j(0);j<=l;j++){
            cov[i][j]=cov_MBS(b_0[0][0][0]+h*i,b_0[0][0][0]+h*j,H[0]);
        }
    }
    decomp_mat_tri_inf(cov , tri_inf);
    b_0.clear();
    b_0.push_back(tab);
    double s;
    
    for(int i(1);i<=iter;i++){
        t_min=t_min+h;
        tab[0]=vector<double>(dimension,t_min);
        for(int j=1;j<=l;j++){
            
            for(int k=0;k<dimension;k++){
                s=0;
                for(int m(0);m<j;m++){
                    tab[j][k]+=tri_inf[i][m]*N[(i-1)*l*k+m-1+k];
                }
                
                
            
            }
        }
        
        b_0.push_back(tab);
    }
    
}

