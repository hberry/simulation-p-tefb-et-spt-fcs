//
//  MB_Compact.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 01/06/2021.
//

#ifndef MB_Compact_hpp
//#define MB_Compact_hpp

#include "Biblio_generateur_alea.hpp"
#include <stdio.h>

void sim_compact_mb_d1_reflexion(vector<vector<double>>& traj, double t_min, double t_max, double b_0, double D, int iter,double sup,double inf);
void sim_n_compact_mb_d1_reflexion(double t_min, double t_max, vector<vector<double>>& b_0, double D, int iter,double sup,double inf);
void sim_n_mb_d_reflexion_polyedre(double t_min, double t_max, vector<vector<vector<double>>>& b_0, vector<double> D, long iter,vector<double> sup,vector<double> inf);

#endif /* MB_Compact_hpp */
