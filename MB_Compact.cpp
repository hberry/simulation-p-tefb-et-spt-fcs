//
//  MB_Compact.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 01/06/2021.
//

#include "MB_Compact.hpp"

using namespace std;

void sim_compact_mb_d1_reflexion(vector<vector<double>>& traj, double t_min, double t_max, double b_0, double D, int iter,double sup,double inf){  // simule un brownien tq B_t_min=b_0 jusqu'à t_max en iter pas. colone 0 les instants colonnes 1 les valeur du brownien il sera piégé entre inf et sup
    //attention il faut que b_0 soit entre inf et sup
    //la traj rebondie sur le bord
    
    double h=double((t_max-t_min)/iter);
    vector<double> N;
    n_polaire(N, iter,0, 1);
    double a;
    
    traj.clear();
    vector<double>tab ={t_min,b_0};
    traj.push_back(tab);
    for(int i(1);i<=iter;i++){
        t_min=t_min+h;
        a=b_0+sqrt(D*h)*N[i-1];
        while(a<inf or a>sup){
            if(a<inf){
                
                a=2*inf-a;
            }
            if(a>sup){
                
                a=2*sup-a;
            }
        }
        
        b_0=a;
        
        vector<double>tab ={t_min,b_0};
        traj.push_back(tab);
    }
    
}

void sim_n_compact_mb_d1_reflexion(double t_min, double t_max, vector<vector<double>>& b_0, double D, int iter,double sup,double inf){  // simule des brownien tq B_t_min=b_0 (vecteur) jusqu'à t_max en iter pas. colone 0 les instants colonnes 1 les valeur du brownien
    
    double h=(t_max-t_min)/iter;//le pas
    vector<double> N;//nos simulation de loi normale
    long l=b_0[0].size(); //le nombre de simu qu'on veut
    n_polaire(N, iter*l,0, 1);
    double a;
    
    vector<double>tab (l+1,t_min);
        
    for(int i=1;i<=l;i++){ //j'initialise t0 x0
        tab[i]=b_0[0][i-1];
    }
    
    b_0.clear();
    b_0.push_back(tab);
    
    
    for(int i(1);i<=iter;i++){
        t_min=t_min+h;
        tab[0]=t_min;
        for(int j=1;j<=l;j++){
            
            a=tab[j]+sqrt(D*h)*N[(i-1)*l+j-1];
            while(a<inf or a>sup){
                if(a<inf){
                    
                    a=2*inf-a;
                }
                if(a>sup){
                    
                    a=2*sup-a;
                }
            }
            
            tab[j]=a;
        }
        
        b_0.push_back(tab);
    }
    
}

void sim_n_mb_d_reflexion_polyedre(double t_min, double t_max, vector<vector<vector<double>>>& b_0, vector<double> D, long iter,vector<double> sup,vector<double> inf){  // simule des brownien tq B_t_min=b_0(vecteur de vecteur de dim d)  jusqu'à t_max en iter pas. colone 0 les instants colonnes i>0 les valeur du brownien
    
    long dimension=b_0[0][0].size();
    double h=(t_max-t_min)/iter;//le pas
    vector<double> N;//nos simulation de loi normale
    long l=b_0[0].size(); //le nombre de simu qu'on veut
    n_polaire(N, iter*l*dimension,0, 1);
    double a;
    
    vector<vector<double>>tab (l+1,vector<double> (dimension,t_min));//je déclare mon vecteur de construction
     
    if(dimension==sup.size() and dimension==inf.size()){ //on s'assure que les paramètres sont correct sinon on ne fait rien
        for(int i=1;i<=l;i++){ //j'initialise t0 x0
            tab[i]=b_0[0][i-1]; //j'initialise mon vecteur de construction
        }
        
        b_0.clear();
        b_0.push_back(tab);
        
        for(int i(1);i<=iter;i++){
            t_min=t_min+h;
            tab[0]=vector<double>(dimension,t_min);
            for(int j=1;j<=l;j++){
                for(int k=0;k<dimension;k++){
                    
                    a=tab[j][k]+sqrt(D[k]*h)*N[(i-1)*l*k+j-1+k];
                    while(a<inf[k] or a>sup[k]){
                        if(a<inf[k]){
                            
                            a=2*inf[k]-a;
                        }
                        if(a>sup[k]){
                            
                            a=2*sup[k]-a;
                        }
                    }
                    
                    tab[j][k]=a;
                }
            }
            
            b_0.push_back(tab);
        }
    }
    
        
    }
