//
//  tools.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 31/05/2021.
//

#include "tools.hpp"

using namespace std;

double carre(double a){
    return(a*a);
}

int variation_quadratique_traj(vector<vector<double>> donnees, vector<vector<double>>& instants){//fais la variation quadra aux instants donnes attentions instant est un vecteur 2x2 colonnes 0 les temps colonne 1 peu importe
    
    
    int existence_t_i=0;
    long l=instants.size();
    vector<double>M(l,0);
    int i(0);
    int j(0);
    
    while(i<donnees.size() and j<instants.size()){
        if(donnees[i][0]==instants[j][0]){
            instants[j][1]=carre(donnees[i][1]-donnees[0][1]);
            
            j++;
        }
        i++;

    }
    if(j==instants.size()){
        existence_t_i=1;//renvoie 1 si il y a tous les instants
    }
    
    return(existence_t_i);
}

int msd_MC_d1(vector<vector<double>> data, vector<vector<double>>& instant){ //renvoie un vecteur 2x2 avec colonne 0 les temps et colonne 1 les msd attention bien donner instant un vecteur 2x2 colonnes 0 les temps colonne 1 peu importe
    int existence_t_i=0;
    
    double s=0;
    long nombre_simu=data[0].size()-1;
    
    int i(0);
    int j(0);
    
    while(i<data.size()and j<instant.size()){
        if(data[i][0]==instant[j][0]){
            for(long l=1;l<=nombre_simu;l++){
                s=s+carre(data[i][l]-data[0][l]);
            }
            instant[j][1]=s/nombre_simu;
            s=0;
            
            j++;
        }
        i++;

    }
    if(j==instant.size()){
        existence_t_i=1;//renvoie 1 si il y a tous les instants
    }
    
    return(existence_t_i);

}


int msd_MC(vector<vector<vector<double>>> data, vector<vector<double>>& instant){ //renvoie un vecteur 2x2 avec colonne 0 les temps et colonne 1 les msd attention bien donner instant un vecteur 2x2 colonnes 0 les temps colonne 1 peu importe
    int existence_t_i=0;
    long dimension=data[0][0].size();
    
    
    double s=0;
    long nombre_simu=data[0].size()-1;
    
    int i(0);
    int j(0);
    
    while(i<data.size()and j<instant.size()){
        if(data[i][0][0]==instant[j][0]){
            for(long l=1;l<=nombre_simu;l++){
                for(int k=0;k<dimension;k++){//norme euclidienne au carré
                    s=s+carre(data[i][l][k]-data[0][l][k]);
                }
            }
            instant[j][1]=s/nombre_simu;
            s=0;
            
            j++;
        }
        i++;

    }
    if(j==instant.size()){
        existence_t_i=1;//renvoie 1 si il y a tous les instants
    }
    
    return(existence_t_i);

}

double norm(vector<double>X){
    double S=0;
    for(int i(0);i<X.size();i++){
        S=S+carre(X[i]);
    }
    return(sqrt(S));
}
double norm(double X[dimension]){
    double S=0;
    for(int i(0);i<dimension;i++){
        S=S+carre(X[i]);
    }
    return(sqrt(S));
}



void reflexion_boule_d2(vector<double>& X0,vector<double>& X1,double  a){ //fais la reflexion de X0 in E et X1 out E avec E boule de rayon a    voir p 09/07/2021 pour les calculs  PB pour x0=0 et X1=sqrt2 sqrt2
    double B=2*(ps(X0,X1)-carre(norm(X1)));
    double A=carre(norm(X0))+carre(norm(X1))-2*ps(X0,X1);
    double C=carre(norm(X1))-carre(a);
    double delta=carre(B)-4*A*C; //mon discriminant pour t star
    double t=-(B+sqrt(delta))/(2*A);
    if(t<0 or t>1){
        t=-(B-sqrt(delta))/(2*A);
    }
    
    vector<double> x_star(2,0); //point d'intersection entre X0-X1 et la bordure de la boule
    x_star[0]=t*X0[0]+(1-t)*X1[0];
    x_star[1]=t*X0[1]+(1-t)*X1[1];
    
    
    X1[0]=X1[0]-x_star[0];
    X1[1]=X1[1]-x_star[1];
    
    double l=norm(X1);
    
    vector<double>gradH(2,0);
    gradH[0]=2*x_star[0];
    gradH[1]=2*x_star[1];
    
    X0[0]=X0[0]-x_star[0];
    X0[1]=X0[1]-x_star[1];
    double theta=-1*1/(norm(X0)*norm(gradH))*ps(X0,gradH);
    if(theta>1){
        theta=1;
    }
    if(theta<-1){
        theta=-1;
    }
    theta=2*acos(theta);
    
   //sens horaire
    vector<double> util(2,0);
    
    util[0]=X0[0]*cos(theta)+X0[1]*-sin(theta);
    util[1]=X0[0]*sin(theta)+X0[1]*cos(theta);
    
    A=norm(util);
    X1[0]=util[0]/A*l+x_star[0];
    X1[1]=util[1]/A*l+x_star[1];
    
    util[0]=X1[0]-x_star[0];
    util[1]=X1[1]-x_star[1];
    
    
    if(ps(util,gradH)>0){//sens anti horaire
        util[0]=X0[0]*cos(theta)+X0[1]*sin(theta);
        util[1]=X0[0]*-sin(theta)+X0[1]*cos(theta);
        A=norm(util);
        X1[0]=util[0]/A*l+x_star[0];
        X1[1]=util[1]/A*l+x_star[1];
    }
}

double ps(vector<double>X,vector<double>Y){
    double S=0;
    for(int i(0);i<X.size();i++){
        S=S+X[i]*Y[i];
    }
    return(S);
}

void regression_alpha(vector<vector<double>>& instant){ //je commence à la ligne 1 pour éviter d'avoir ln 0 quand mon proc part de 0 en 0
    double alpha=0;
    double bar_x=0;
    double bar_y=0;
    double var=0;
    
    for(int i(0);i<instant.size()-1;i++){
        instant[i+1][0]=log(instant[i+1][0]);
        instant[i+1][1]=log(instant[i+1][1]);
        bar_x=bar_x+instant[i+1][0];
        bar_y=bar_y+instant[i+1][1];
    }
    bar_x=bar_x/(instant.size()-1);
    bar_y=bar_y/(instant.size()-1);
    
    for(int i(0);i<instant.size()-1;i++){
        alpha=alpha+(instant[i+1][0]-bar_x)*(instant[i+1][1]-bar_y);
        var=var+carre(instant[i+1][0]-bar_x);
    }
    instant.clear();
    instant[0][0]=alpha/var;
    instant[0][1]=exp(bar_y-instant[0][0]*bar_x);
}


void decomp_mat_tri_inf(vector<vector<double>>A, vector<vector<double>>&L){

        float s=0;
    vector<double>v(A.size(),0);
    
    for(int j(0);j<A.size();j++){
        for(int i(0);i<j-1;i++){
            v[i]=A[j][i]*A[i][i];
        }
        s=0;
        for(int k=0;k<j-1;k++){
            s=s+A[j][k]*v[k];
        }
        A[j][j]=A[j][j]-s;
        
        for(int i(j+1);i<A.size();i++){
            s=0;
            for(int k(0);k<j-1;k++){
                s=s+A[i][k]*v[k]/v[j];
                }
            A[i][j]=A[i][j]-s;
        }
    }
    for(int i(0);i<A.size();i++){
        for(int j(0);j<A.size();j++){
            if(i>j){
                L[i][j]=A[i][j]*sqrt(A[i][i]);
            }
            if(i==j){
                L[i][j]=sqrt(A[i][j]);
            }
        }
    }
    
        
       /* for(int j(0);j<A.size();j++){
            s=0;
            for(int k=0;k<j;k++){
                s=carre(L[j][k])*A[k][k];
            }
            A[j][j]=A[j][j]-s;
            
            for(int i(j+1);i<A.size();i++){
                s=0;
                for(int k(0);k<j;k++){
                    s=L[i][k]*L[j][k]*A[k][k];
                    }
                L[i][j]=(A[i][j]-s)/A[j][j];
            }
        }
        
        for(int i(0);i<A.size();i++){
            for(int j(0);j<A.size();j++){
                if(i==j){
                    L[i][j]=1;
         
                    }
                L[j][i]*=sqrt(A[i][i]);
            }
        }*/
}

void cholesky(vector<vector<double> >& data){
    // Logic taken from https://en.wikipedia.org/wiki/Cholesky_decomposition#The_Cholesky.E2.80.93Banachiewicz_and_Cholesky.E2.80.93Crout_algorithms
    // Calculation performed via the Cholesky-Banachiewicz (row-by-row) algorithm

    // Initialize the output matrix
    vector<vector<double>> L(data.size(), vector<double>(data[0].size(), 0));

    // Set the first diagonal value equal to the square root of the first diagonal of the input matrix
    L[0][0] = sqrt(data[0][0]);

    // Iterate through rows
    for (int i = 1; i < L.size(); ++i)
    {
        // Iterate through columns
        for (int j = 0; j <= i; ++j)
        {
            // Create sum variable to account for summed "prior" entries that may be necessary to include
            float sum_elements = 0;
            // Diagonal entry logic
            if (i == j)
            {
                // Calculate the squares of prior entries in the i-th row
                for (int k = 0; k < j; ++k)
                {
                    sum_elements += carre(L[j][k]);
                }
                // Input diagonal value minus sum of squares of other row entries
                L[i][j] = sqrt(data[i][j] - sum_elements);
            }
            else
            {
                // This part was included to deal with the "k < j" part of the loop.  If we're at the first column of a given row then "sum_elements" should stay = 0
                if (j != 0)
                {
                    for (int k = 0; k < j; ++k)
                    {
                        sum_elements += (L[i][k] * L[j][k]);
                    }
                }
                L[i][j] = (1 / L[j][j]) * (data[i][j] - sum_elements);
            }
        }
    }
    
    data=L;
}
double cov_MBS(double s,double t, double H){
    return(0.5*(pow(abs(s),2*H)+pow(abs(t),2*H)-pow(abs(t-s),2*H)));
}
double cov_MBS(double s,double t, double H[dimension]){
    
    for (int i(0);i<dimension;i++){
        H[0]+=1/pow(2,dimension)*(pow(abs(s),2*H[i])+pow(abs(t),2*H[i])-pow(abs(t-s),2*H[i]));
    }
    return(H[0]);
}

void equal(double x_0[dimension],double x_1[dimension]){
    for(int i(0);i<dimension;i++){
        x_0[i]=x_1[i];
    }
}

void equal2(double x_0[2],double x_1[2]){
    for(int i(0);i<2;i++){
        x_0[i]=x_1[i];
    }
}



double ps(double X[3],double Y[3]){
    double S=0;
    for(int i(0);i<3;i++){
        S=S+X[i]*Y[i];
    }
    return(S);
}



void reflexion_boule_d2(double X0[2],double X1[2],double  a){ //fais la reflexion de X0 in E et X1 out E avec E boule de rayon a  retourne le résultat dans X0  voir p 09/07/2021 pour les calculs  PB pour x0=0 et X1=sqrt2 sqrt2
    double B=2*(ps(X0,X1)-carre(norm(X1)));
    double A=carre(norm(X0))+carre(norm(X1))-2*ps(X0,X1);
    double C=carre(norm(X1))-carre(a);
    double delta=carre(B)-4*A*C; //mon discriminant pour t star
    double t=-(B+sqrt(delta))/(2*A);
    if(t<0 or t>1){
        t=-(B-sqrt(delta))/(2*A);
    }
    
    double x_star[2]={0,0}; //point d'intersection entre X0-X1 et la bordure de la boule
    x_star[0]=t*X0[0]+(1-t)*X1[0];
    x_star[1]=t*X0[1]+(1-t)*X1[1];
    
    
    X1[0]=X1[0]-x_star[0];
    X1[1]=X1[1]-x_star[1];
    
    double l=norm(X1);
    
    double gradH[2]={0,0};
    gradH[0]=2*x_star[0];
    gradH[1]=2*x_star[1];
    
    X0[0]=X0[0]-x_star[0];
    X0[1]=X0[1]-x_star[1];
    double theta=-1*1/(norm(X0)*norm(gradH))*ps(X0,gradH);
    if(theta>1){
        theta=1;
    }
    if(theta<-1){
        theta=-1;
    }
    theta=2*acos(theta);
    
   //sens horaire
    double util[2]={0,0};
    
    util[0]=X0[0]*cos(theta)+X0[1]*-sin(theta);
    util[1]=X0[0]*sin(theta)+X0[1]*cos(theta);
    
    A=norm(util);
    X1[0]=util[0]/A*l+x_star[0];
    X1[1]=util[1]/A*l+x_star[1];
    
    util[0]=X1[0]-x_star[0];
    util[1]=X1[1]-x_star[1];
    
    
    if(ps(util,gradH)>0){//sens anti horaire
        util[0]=X0[0]*cos(theta)+X0[1]*sin(theta);
        util[1]=X0[0]*-sin(theta)+X0[1]*cos(theta);
        A=norm(util);
        X1[0]=util[0]/A*l+x_star[0];
        X1[1]=util[1]/A*l+x_star[1];
    }
}


long factorielle(int valeur){
   if (valeur == 0)
      return 1;
   else
      return valeur * factorielle(valeur - 1);
}

double distance(double x[dimension],double y[dimension]){
    double s=0.0;
    for(int i(0);i<dimension;i++){
        s+=carre(x[i]-y[i]);
    }
    return(sqrt(s));
}


void moins(double x[dimension],double y[dimension]){ //stocks in x=x-y
    for(int i(0);i<dimension;i++){
        x[i]-=y[i];
    }
}

void vplus(double x[dimension],double y[dimension]){ //stocks in x=x+y
    for(int i(0);i<dimension;i++){
        x[i]+=y[i];
    }
}


double gaussian_intensity(double x[3],double sigma[3]){
    //double p=PI;
    //DiffHB: corrected the 1/sigma[0] at the beginning, and removed the prefactor (that is now defined by "illumination")
    //return(pow(2*p,-dimension/2)*(1/sigma[0]*sigma[1]*sigma[2])*exp(-(carre(x[0]/sigma[0])+carre(x[1]/sigma[1])+carre(x[2]/sigma[2]))/2));
    return(exp(-2.0*(carre(x[0]/sigma[0])+carre(x[1]/sigma[1])+carre(x[2]/sigma[2]))));
}

double equation_ellipse(double X[3],double  sigma[3]){
    return(carre(X[0]/sigma[0])+carre(X[1]/sigma[1])+carre(X[2]/sigma[2]));
}

void reflexion_ellipsoid_d3(double X0[3],double X1PRIME[3],double  sigma[3],double rayon){
    double Y[3];
    double X1[3];//point de reflexion tq ||X1PRIME-Z||=||X0-Z||
    double Z[3];//point de milieu entre X0 et x1PRIME
    double n[3];//vecteur normal
    double calcul[3];
    double a;
    double b;
    double c;
    rayon=carre(rayon);
    double delta;
    double t;
    
    //Détermination de Y point de sortie entre X0 et X1
    
    for(int i(0);i<3;i++){
        calcul[i]=X0[i]-X1PRIME[i];
    }
    a=equation_ellipse(calcul, sigma);
    b=-(equation_ellipse(calcul, sigma)-equation_ellipse(X0, sigma)+equation_ellipse(X1PRIME, sigma));
    c=equation_ellipse(X1PRIME, sigma)-rayon;
    delta=carre(b)-4.0*a*c;
    t=(-b+sqrt(delta))/(2.0*a);
   if(t>1 or t<0){
        t=(-b-sqrt(delta))/(2.0*a);;
    }
    
    for(int i(0);i<3;i++){
        Y[i]=t*X0[i]+(1-t)*X1PRIME[i];
        n[i]=carre(Y[i])/sigma[i];
    }
    
    
    //Determination Z tq ||X1PRIME-Z||=||X0-Z|| et ||Y-Z||^2+||X0-Z||^2=||X0-Y||^2 et (Y-Z)=lambda*n
    
    
    delta=-(ps(X0,n)-ps(Y,n))/(carre(norm(n)));
    
   
    
    for(int i(0);i<3;i++){
        Z[i]=Y[i]-delta*n[i];
    }
    
    //X1=(Z-X0)+Z
    for(int i(0);i<3;i++){
        X1[i]=2*Z[i]-X0[i];
    }
    
    //X"-Y-t(X1-Y)  or ||X1PRIME-Y||=||X"-Y||
    
    t=distance(X1PRIME, Y)/distance(X1, Y);
    if(t<0){
        t=-t;
    }
    
    for(int i(0);i<3;i++){
        X1PRIME[i]=t*(X1[i]-Y[i])+Y[i];//met le résultat dans X0
        
       X0[i]=Y[i];
    }
    
    
}

