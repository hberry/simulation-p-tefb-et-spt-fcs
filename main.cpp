//
//  main.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 26/05/2021.
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include<cstdlib>
#include<cmath>
#include<time.h>
#include <fstream>
#include <sstream>


#include "Biblio_generateur_alea.hpp"
#include "tests.hpp"
//#include "test_simu_objet.hpp"
#include "simu_vector_objet.hpp"
#include "FCS_GENERATOR.hpp"

#include <Accelerate/Accelerate.h>

#define ROOT_PATH "outputs/"

using namespace std;

//// DiffHB: added C++ STL random generators as global variables
#include <random>
//Random generators
mt19937_64 rng;
uniform_real_distribution<double> rand_ureal_01(0.0,1.0);//real Uniform in [0,1)
normal_distribution<double> rand_norm(0,1);//Strandard Normal(0,1) distribution


int main(int argc, const char * argv[]) {
    
    srand(time(NULL));
    double eps=0.01;
    int valid=1;
    double D[3]={3.2e-7,3.2e-7,3.2e-7};//mm2.sec-1. Typical is 10 µm^2/sec, ie 10e-6 mm2/sec
    double H=0.1;
    long iter=1e6;
    double t_min=0;
    double t_max=10;//sec
    double nombre_simu=1;
    double b_0[3]={0,0,0};
    double alpha=0.20;
    double alphabeta[2];
    double sigma_1d=1;
    double rayon=2.0;
    double illumination=5e3;//20e4;
    //double nombre_particule=20;
    double mean_emission_before_bleaching=1e8;
    double pas=(t_max-t_min)/iter;
    //// DiffHB: sigma was sometimes used for the std of the distrib of the spatial jumps but also for the beam waist (\omega_x,\omega_y, \omega_z)
    //// found it confusing. So I let sigma for the std of jumps and use now omega for the beam waist
    double sigma[3]={0.1,0.1,0.1};//{0.0005,0.0005,0.0005};//{10,10,10};
    //DiffHB: warning, omegas are hundreds of nms, so around 0.1e-3 mm
    double omega[3]={0.172e-3,0.172e-3,0.8084e-3};
    int rngseed=0;

    //// DiffHB: added input parameters passed from commmand line
    //input parameters
    if (argc==5)
    {   
        alpha=atof(argv[1]);
        double sigma_in=atof(argv[2]);
        sigma[0]=sigma_in;
        sigma[1]=sigma_in;
        sigma[2]=sigma_in;
        pas=atof(argv[3]);//with 100 points per trajectories, best is dt=1e-3 for tau=1e-6
        t_max=pas*iter;
        rngseed=atoi(argv[4]);
    }
    else
    {
        cout<<"--------------------------------- "<<endl;
        cout<<"Using parameters by default "<<endl;
        cout<<"consider using ./ABC4M alpha sigma dt seed"<<endl;
        cout<<"--------------------------------- "<<endl;
    }


    double nombre_particule=8;
    double timewindow=0.1;

    double rayon_noyau=0.00125;//0.00125;//0.01;//mm
    double rayon_obs=0.0005;//mm
    double quantité_simu_obs=nombre_simu*1000000;
    double longueur_mini_obs=10;
    
    double sigma_obs[3]={rayon_obs,rayon_obs,rayon_obs};
    
    //diffHB: the total number of particles in the noyau must be scaled in relation to how many particle we want in 
    //the effective sampling volume Ve=\pi^{3/2} \omega_x\omega_y \omega_z 
    //long nombre_particule_dans_noyau=long(floor(nombre_particule*pow(rayon_noyau/rayon_obs,3)*(4/(3*sqrt(3.14)))*(carre(sigma[2])/(sigma[0]*sigma[1])) ));
    long nombre_particule_dans_noyau=long(floor(nombre_particule*pow(rayon_noyau,3)*4/(3*sqrt(3.14159)*omega[0]*omega[1]*omega[2])));
    cout<<"Nombre de particules par FCS="<<nombre_particule_dans_noyau<<endl;
    cout<<"rayon noyau ="<<rayon_noyau<<endl;
    cout<<"D="<<D[0]<<","<<D[0]<<","<<D[0]<<endl;
    cout<<"lambda="<<illumination*nombre_particule_dans_noyau*gaussian_intensity(D,sigma)/iter<<endl;
    //cout<<"On a masqué les photon dans une bioule de rayon inférioeur à 0.0001"<<endl;
    std::stringstream ss;
    

    vector<vtrajectoire>simus;
    vector<vtrajectoire>copy;
    vector<quantities>MSD_test_pop;
    vector<quantities>MSD_test_traj;
//    vector<vector<double>> test(iter,vector<double> (iter,0));
    vector<FCS_train> test_photons;
    vector<quantities> result;
    vector<vquantities> TAMSD;
    vector<double> temps_obs;
    vector<double> FCS_test;
    FCS_train FCTEST(quantities(0,0));
   
  
    
    
    rng.seed(rngseed);


    std::stringstream uu;


    for(int i(0);i<=iter;i++){
        temps_obs.push_back(i*pas);
    }
    
    // double res[3]={0,0,0};
    // for(int i(0);i<=1e6;i++){
    //     plus_polaire_3(res, sigma);
    //     uu<<res[0]<<","<<res[1]<<","<<res[2]<<","<<endl;
    //     res[0]=0;res[1]=0;res[2]=0;

    // }
    // std::string u = uu.str();
    // ofstream filetest(ROOT_PATH "test_ctrw.csv", std::ofstream::out);
    // if (filetest.bad()) cout<<"ok"<<endl;
    // filetest << u<< endl;
    // filetest.close();

    //Génération des simus dans un vector de vtrajectoire
    cout<<"Generating trajectories"<<endl;
    for(int i(0);i<nombre_simu*nombre_particule_dans_noyau;i++){
        //test_photons.push_back(FCS_train(quantities(1, 0)));
        simus.push_back(ov_sim_free_mb_d(D,particule(b_0,0),iter,pas));
        //simus.push_back(ov_sim_free_mbf(particule(b_0,0),iter,pas, H,D));
        //simus.push_back(ov_sim_free_ctrw_puis(alpha, sigma, temps_obs, particule(b_0,0)));
        //simus.push_back(ov_sim_free_ctrw_expo(alpha, sigma, temps_obs, particule(b_0,0)));
        /*for(int j(1); j<iter;j++){
            test_photons.back().observations.push_back(quantities(pow(-1,j),j*pas));
        }*/
        
        
       
    }
    
    
    cout<<"total number of trajectories "<<simus.size()<<endl;
    
  
    //Générateur de FCS a partir des simus dans un vector de FCS_train
   cout<<"Generating photons trains"<<endl;
   test_photons=generate_normalised_FCS_train_MC(simus,omega,rayon_obs,t_max,illumination,mean_emission_before_bleaching,nombre_particule_dans_noyau,pas,rayon_noyau);
   // cout<<"FCS ok let's watch it"<<endl;
   // for(int i(0);i<test_photons[0].observations.size();i++){
   //    cout<<test_photons[0].observations[i].time<<"  =  "<<test_photons[0].observations[i].space<<endl;
   // }
   // cout<<test_photons.size()<<endl;
    
    // result=autocorrelation_function(test_photons);
    // cout<<"autocorrelation ok let's watch it"<<endl;
    // for(int i(0);i<result.size();i++){
    //     cout<<result[i].time<<"  =  "<<result[i].space<<endl;
    // }
    
    
    
    
  
    
    
    
    //Tester la réflexion ellipse
    /*
    double s0;
    double s1;
    double sr;
    double theta=1;
    
    double RX[3][3]={{1,0,0},{0,cos(theta),-sin(theta)},{0,sin(theta),cos(theta)}};
    
    double X0[3]={sqrt(2)/2,0,0};
    double X1[3]={sqrt(2)/2,sqrt(2),0};
    double Result[3]={0,sqrt(2)/2,0};
    
    
    for(int i(0);i<3;i++){
        s0=0;
        s1=0;
        sr=0;
        for(int j(0);j<3;j++){
            s0+=X0[j]*RX[i][j];
            s1+=X1[j]*RX[i][j];
            sr+=Result[j]*RX[i][j];
        }
        X0[i]=s0;
        X1[i]=s1;
        Result[i]=sr;
        
    }
    
    for(int i(0);i<3;i++){
        cout<<X0[i]<<"    "<<X1[i]<<" Result="<<Result[i]<<endl;
    }
    
    
    reflexion_ellipsoid_d3(X0,X1,sigma,1);
    
    for(int i(0);i<3;i++){
        cout<<X0[i]<<"    "<<X1[i]<<" Result="<<Result[i]<<endl;
    }
    
    cout<<"sqrt(2)"<<" = "<<sqrt(2)<<endl;
*/
    //Test réflexion d3
    /*
    double SIGMA[3]={1.0,1.0,1.0};
    double X0[3]={0,0,-sqrt(2)/2};
    double X1Y[3]={0,sqrt(2),-sqrt(2)/2};
    
    
    reflexion_boule_d3(X0,X1Y,SIGMA,1.5);
    
    for(int i(0);i<dimension;i++){
        cout<<X0[i]<<"   "<<X1Y[i]<<endl;
    }
    
    cout<<"sqrt(2)/2= "<<sqrt(2)/2<<endl;
    cout<<"sqrt(2)/4= "<<sqrt(2)/4<<endl;
    cout<<"sqrt(2)= "<<sqrt(2)<<endl;
    cout<<"sqrt(3)/2= "<<sqrt(3)/2<<endl;
    cout<<"3/(2*sqrt(2))= "<<3/(2*sqrt(2))<<endl;
    
    */
    
    
   //Générateur temps obs et simus
    /*
    for(int i(0);i<=iter;i++){
        temps_obs.push_back(i*pas);
    }
   
    
    
    for(int i(0);i<nombre_simu;i++){
        
       
        simus.push_back(ov_sim_free_mb_d(D,particule(b_0,0),iter,pas));
        //simus.push_back(ov_sim_boule_mb_d2(D,particule(b_0,0),iter,pas,rayon_noyau));
        //simus.push_back(ov_sim_free_ctrw_puis(alpha, sigma, temps_obs, particule(b_0,0)));
        //simus.push_back(ov_sim_free_ctrw_expo(1, sigma, temps_obs, particule(b_0,0)));
        //cout<<i<<endl;
        random_start_ellipsoid(simus.back(),sigma,rayon);
        reflexion_ellipsoid_d3(simus.back(),rayon,sigma);
        cout<<i<<endl;
    }
    cout<<"Simus achevées"<<endl;
    recenter_vector_vtrajectoire(simus);
    */
    //test FCS train generator
    /*
    FCS_test=generate_FCS_train(simus, sigma,
                                rayon,t_max,illumination,bleaching_poisson);
    
    for(int i(0);i<FCS_test.size();i++){
        cout<<FCS_test[i]<<endl;
    }
    cout<<FCS_test.size()<<endl;
   
*/
    //Tests MSD et régression
    /*
    ov_msd_MC(simus, result);
    for (int i(0);i<result.size();i++){
        cout<<result[i].time<<"  =  "<<result[i].space<<endl;
    }
    ov_regression_alpha(result, alphabeta);
    cout<<alphabeta[0]<<"  et  "<<alphabeta[1]<<endl;
    
    
*/
    
    //cout<<o_test_sim_free_ctrw_puis()<<endl;
    
    
    
    
    //Diffusion brownienne dans noyau génère de la sous diff en obs
    /*
    for(int i(0);i<nombre_simu;i++){
        //simus.push_back(ov_sim_free_mb_d(D,particule(b_0,0),iter,pas));
        simus.push_back(ov_sim_boule_mb_d2(D,particule(b_0,0),iter,pas,rayon_noyau));
        //cout<<i<<endl;
    }
    cout<<"Simus achevées"<<endl;
   
    ov_msd_MC(simus, result);
    for (int i(0);i<=10;i++){
        cout<<result[i].time<<"  =  "<<result[i].space/2*carre(D[0])<<endl;
    }
    ov_regression_alpha(result, alphabeta);
    cout<<alphabeta[0]<<"  et  "<<alphabeta[1]<<endl;
    
    result.clear();
    simus=filtre_observation_dans_boule(simus,b_0,rayon_obs,longueur_mini_obs,quantité_simu_obs);
    //filtre_observation_dans_boule(vector<vtrajectoire> simus_noyau,double centre_observation[dimension],double rayon_obs, int longeur_mini_obs, long quantite_simu_obs);
    cout<<"Simus observations achevéees"<<endl;
    cout<<endl<<"La taille des simus obs est  "<<simus.size()<<endl;
    recenter_vtrajectoire(simus);
    ov_msd_MC(simus, result);
   
    
    
    for (int i(0);i<=10;i++){
        cout<<result[i].time<<"  =  "<<result[i].space/2*carre(D[0])<<endl;
        
    }
    ov_regression_alpha(result, alphabeta);
    cout<<alphabeta[0]<<"  et  "<<alphabeta[1]<<endl;
    
    */
    
    //Test fonction autocorrelation on obtient une certaine reproductibilité
    
    /*
    double timestep=0.1;
     for(int i(0);i<nombre_simu;i++){
         simus.push_back(ov_sim_free_mb_d(D,particule(b_0,0),iter,pas));
         //cout<<i<<endl;
     }
     
     test_photons=generate_normalised_FCS_train_MC(simus, sigma,rayon,t_max,illumination,intensitee,nombre_particule,timestep);
     
     
    
     result=autocorrelation_function(test_photons);
     
     for (int i(0);i<10;i++){
         cout<<result[i*iter/10].time<<"  =  "<<result[i*iter/10].space<<endl;
     }
    */
    
    //Créer un vecteur de simu
    /*
    for(int i(0);i<nombre_simu;i++){
     
        
        simus.push_back(ov_sim_free_mbf(particule(b_0,0),iter,pas, 0.25));
    }
    
    ov_msd_MC(simus, MSD_test_pop);
    ov_msd_traj(simus, MSD_test_traj);
    */
    

    
    //Sortir un fichier csv
    
    
    // long taille_simu=simus[0].observations.size();
  

    // for(int t(0);t<=(taille_simu-1);t++){

    //     for (int tr(0);tr<=nombre_simu;tr++)
    //     {
    //         ss<<simus[tr].observations[t].space[0]<<","<<simus[tr].observations[t].space[1]<<","<<simus[tr].observations[t].space[2]<<",";
    //     }
    //     ss<<endl;
    // }
    
  
   
    // std::string s = ss.str();
    
        
    // ofstream file(ROOT_PATH "data_MBF.csv", std::ofstream::out);
     
        
     
    // if (file.bad())
    //     { cout<<"ok"<<endl;}

    // file << s<< endl;

    // file.close();

   
    
   
    
    
    
    
    
    return 0;
}
