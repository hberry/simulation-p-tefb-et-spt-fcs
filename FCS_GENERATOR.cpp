//
//  FCS_GENERATOR.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 05/08/2021.
//

#include "FCS_GENERATOR.hpp"

//HB: DEBUG
#define ROOT_PATH "outputs/"
#include <fstream>
#include <sstream>

//// DiffHB: added C++ STL random generators as global variables
#include <random>
extern mt19937_64 rng;
extern uniform_real_distribution<double> rand_ureal_01;//real Uniform in [0,1)

//Translate a vtrajectoire
void new_start_sphere(vtrajectoire & traj, long range_particule, double radius){
    
    double exit_position[dimension];
    ///diffHB: we want the trajectory to reappear somewhere on the ball boundary
    double theta=rand_ureal_01(rng)*2*PI;
    double rho=rand_ureal_01(rng)*2*PI;

    double new_start[3]={radius*sin(theta)*cos(rho),radius*sin(theta)*sin(rho),radius*cos(theta)};
    
    equal(exit_position,traj.observations[range_particule].space);
    
    
    
    
    for(int i(range_particule);i<traj.observations.size();i++){
        for(int k(0);k<dimension;k++){
            traj.observations[i].space[k]=traj.observations[i].space[k]-exit_position[k]+new_start[k];
        }
    }
}



void new_start_ellipsoid(vtrajectoire & traj, long range_particule, double sigma[3]){
    
    double exit_position[dimension];
    double theta=rand_ureal_01(rng)*2*PI;
    double rho=rand_ureal_01(rng)*2*PI;
    double new_start[3]={sin(theta)*cos(rho)/sigma[0],sin(theta)*sin(rho)/sigma[1],cos(theta)/sigma[2]};
    
    equal(exit_position,traj.observations[range_particule].space);
    
    
    
    
    for(int i(range_particule);i<traj.observations.size();i++){
        for(int k(0);k<dimension;k++){
            traj.observations[i].space[k]=traj.observations[i].space[k]-exit_position[k]+new_start[k];
        }
    }
}



vector<double> generate_FCS_train(vector<vtrajectoire> trajs, double sigma[3],double radius,double T_max,double illumination,double lambda_poisson_bleach){//voies d'amélioration: générer tous les trains de photo,ns en même temps , gains de performance
    //préparez les traj avant
    //renvoie les temps d'arriver de chaques photons
    int N=trajs.size();
    double step=trajs[0].observations[1].time-trajs[0].observations[0].time;
    double zero[dimension];
    for(int i(0);i<dimension;i++){
        zero[0]=0;
    }
    
    //DiffHB: in the intial version max lambda was initiated with a position zero, ie
    // at max amplitude, if it's not a bug I don't understand it
    //double max_lambda=illumination*N*gaussian_intensity(zero,sigma);
    //double poisson_process=-step;//temps de dernier saut
    //double exp;
    // double uniforme;
    // int indice_photon;
    // vector<int> emission_before_bleaching(N,0);
    // int sum_emission_before_bleaching=0;
    
    
    // vector<double> intensitees(N,0);
    // double intensitee_tot;
    
    vector<double> n;
    n.clear();
    
    
    // for(int i(0);i<N;i++){//init les bleach max
    //     emission_before_bleaching[i]=poisson_law(lambda_poisson_bleach,1);
    //     sum_emission_before_bleaching+=emission_before_bleaching[i];
    // }
    
    
    //Diff HB: I don't get it: why do you add an expo(max_lambda), that is the probability
    //of a photon be emitted at illumination max_lambda to the last jump time?
    //exp=expo(max_lambda);
    //poisson_process+=exp;
    //uniforme=rand_ureal_01(rng)*max_lambda;
    

    //DiffHB: I don't understand what the function does below, 
    //there must be a loop going over all the successive positions of the 
    // trajectories but I don't see it. Probably I'm missing something here but
    // but meanwhile, I prefered to make it as simple as possible

    /*
    while(poisson_process<T_max and 0<sum_emission_before_bleaching) {
        intensitee_tot=0;
        for(int i(0);i<N;i++){
            if(emission_before_bleaching[i]>0){
                //DiffHB: I don't get it: the second input of gaussian_intensity_segment_interpolation
                //is supposed to give the time of position i in the trajectory, not a poisson variable
                // corresponding to the next photon emssion time, ie poisson process
                // Not sure I understand the gaussian_intensity_segment_interpolation function
                // so I used the simplest way to do it
                //intensitees[i]=gaussian_intensity_segment_interpolation(trajs[i],poisson_process,step,sigma,illumination);
                intensitees[i]=illumination*(gaussian_intensity(XN, sigma));
                intensitee_tot+=intensitees[i];
            }
            
        }
        if(uniforme<=intensitee_tot and poisson_process>=0){
            n.push_back(poisson_process);
            indice_photon=unif(intensitees);
            emission_before_bleaching[indice_photon]-=1;
            sum_emission_before_bleaching-=1;
            if(emission_before_bleaching[indice_photon]==0){
                intensitees[indice_photon]=0;
            }
        }
        
        
        exp=expo(max_lambda);
        poisson_process+=exp;
        uniforme=rand_ureal_01(rng)*max_lambda;
        //cout<<"Poisson="<<poisson_process<<endl;
    }
    */  


    ///DiffHB: My implementation, taken from the direct algorithm of the Harrod and Kelton paper
    double lambda=0; 
    double C=0;
    double Sn=0;
    for(int i(0);i<N;i++){
        C=0;
        Sn=-log(rand_ureal_01(rng));
        for (int j=0;j<trajs[i].observations.size();j++){ 
            lambda=illumination*gaussian_intensity(trajs[i].observations[j].space,sigma);
            C+=lambda*step;
            while (Sn<C)
            {
                n.push_back(j*step+(Sn-C+lambda*step)/lambda);
                Sn-=log(rand_ureal_01(rng));
            }

        }

    }  
    sort(n.begin(), n.end());

    return(n);
}

double gaussian_intensity_segment_interpolation(vtrajectoire traj, double time, double step, double sigma[3],double illumination){
    double t_n=traj.observations[floor(time/step)].time;//vérifier + ou - 1
    double t_nplus1=traj.observations[floor(time/step)+1].time;
    double XN[3];
    
    if(t_n==time){
        return(illumination*gaussian_intensity(traj.observations[floor(time/step)].space, sigma));
    }
    else{
        equal(XN, traj.observations[floor(time/step)].space);
        for(int i(0);i<dimension;i++){
            XN[i]=XN[i]*((t_nplus1-time)/(t_nplus1-t_n))+traj.observations[floor(time/step)+1].space[i]*(1-((t_nplus1-time)/(t_nplus1-t_n)));
        }
        return(illumination*(gaussian_intensity(XN, sigma)));
   }
}

void trajectory_preparation(vtrajectoire& traj,double radius){
    for(int i(0);i<traj.observations.size();i++){
        ///diffHB: we want the trajectory to remain in a ball of diameter 2Rnoyau centered on (Rnoyau,Rnoyau,Rnoyau)
        //cause random_start initiates the trajectory in a ball of diameter 2Rnoyau centered on (Rnoyau,Rnoyau,Rnoyau)
        //if(norm(traj.observations[i].space)>radius){//on a une hypothèse de champs moyens
        
        if(norm(traj.observations[i].space)>radius){
            new_start_sphere(traj, i, radius);
        }
    }
}

void trajectory_preparation_ellipsoid(vtrajectoire& traj,double sigma[3],double radius){
    for(int i(0);i<traj.observations.size();i++){
        if(norm(traj.observations[i].space)>radius){//on a une hypothèse de champs moyens
            new_start_ellipsoid(traj, i, sigma);
        }
    }
}

FCS_train initialise_FCS_empty(long length,double step){//return a FCS train with no photon at each time the real length is length+1
    FCS_train n(quantities(0,0));
   // n.observations.reserve(length);
    for(long i(1);i<=length;i++){
        n.observations.push_back(quantities(0,i*step));
    }
    return(n);
}

void random_start(vtrajectoire& traj, double radius){
    double random_start[3];
    unif_boule(random_start,radius);
    //diffHB: random_start was initiating the traj in a ball centered on Rnoyau (ie in [0, 2*Rnoyau]). 
    // I rescaled unif_boule so that it now yields a ball centered on 0, in [-Rnoyau, Rnoyau]
    for(int i(0);i<traj.observations.size();i++){
        for(int k(0);k<dimension;k++){
            traj.observations[i].space[k]+=random_start[k];
        }
    }
}
void random_start_ellipsoid(vtrajectoire& traj, double sigma[3],double radius){
    double random_start[3];
    unif_ellipsoid(random_start,sigma,radius);
    
    
    for(int i(0);i<traj.observations.size();i++){
        for(int k(0);k<dimension;k++){
            traj.observations[i].space[k]+=random_start[k];
        }
    }
    
}

vector<FCS_train> generate_normalised_FCS_train_MC(vector<vtrajectoire> simus, double omega[3],double radius,double T_max,double illumination,double mean_emission_before_bleaching,long nombre_particule_dans_noyau,double timestep,double rayon_noyau){//on fait une moyenne sur la pop et le temps on suppose que la moyenne et la var reste constante en temps
    double step=simus[0].observations[1].time-simus[0].observations[0].time;
   
    double nombre_simus=simus.size();
    long nombre_train=floor(nombre_simus*1.0/nombre_particule_dans_noyau);

    double s=0;
    double Mean=0;
    double Var=0;
    
    
    vector<FCS_train> experiment;
    FCS_train constructeur(quantities(0,0));
    vector<vtrajectoire> Constructor;
    vector<double> photons_trains;
    
    
    for(long i(0);i<nombre_simus;i++){
        //diffHB: the trajectories must be intiated withn the noyau, not within sigma*rayon_noyau
        cout<<"preparing trajectory #"<<i<<endl;
        random_start(simus[i],rayon_noyau);
        trajectory_preparation(simus[i],rayon_noyau);
//        random_start(simus[i],sigma[2]*rayon_noyau);// car c'est le plus grand sigma par rapport à z
//        trajectory_preparation(simus[i],sigma[2]*rayon_noyau);
    }
    
    std::stringstream ss;
    //for (int t=0;t<simus[0].observations.size();t++)
    //{
        for (int tr(0);tr<(nombre_simus-1);tr++)
        {
            //ss<<simus[tr].observations[t].space[0]<<","<<simus[tr].observations[t].space[1]<<","<<simus[tr].observations[t].space[2]<<",";
            for (int k(0);k<3;k++)
            {
                ss<<simus[tr].observations[0].space[k]<<endl;
            }

        }
        //ss<<simus[nombre_simus-1].observations[t].space[0]<<","<<simus[nombre_simus-1].observations[t].space[1]<<","<<simus[nombre_simus-1].observations[t].space[2]<<endl;
    //}
    std::string sf = ss.str();
    ofstream file(ROOT_PATH "data_test.csv", std::ofstream::out);
    file << sf<< endl;
    file.close();
    cout<<"(nombre_train,nombre_particule_dans_noyau)="<<nombre_train<<","<<nombre_particule_dans_noyau<<endl;
   for(int i(0);i<nombre_train; i++){
       Constructor.clear();
       for(int j(0);j<nombre_particule_dans_noyau;j++){
           //DiffHB: in the intial version simus.back() was sending always the same vtrajector to Constructor, ie the last one
            //since simus.back() does not remove the last element from simus. I added simus_pop_back to do it
           Constructor.push_back(simus.back());
           simus.pop_back();
       }
       photons_trains=generate_FCS_train(Constructor, omega, radius, T_max, illumination, mean_emission_before_bleaching);
       //experiment.push_back(bin_FCS_train(photons_trains,timestep,T_max));
       cout<<i+1<<" train generated"<<endl;
    }

    //HB: DEBUG: 
    std::stringstream train;
    for(int t(0);t<photons_trains.size();t++){
        train<<setprecision(9)<<photons_trains[t]*1e9<<endl;
    }
    std::string trs = train.str();
    
    ofstream file_t(ROOT_PATH "FCS_train.csv", std::ofstream::out);
    
    file_t << trs<< endl;
    file_t.close();


    
    
    cout<<"all FCS files saved"<<endl;
    //DEBUG

    //DEBUG
    /*
    double iter=experiment[0].observations.size();
    
    for (int i(0);i<iter;i++){
        s=0;
        for(int j(0);j<nombre_train;j++){
            s+=experiment[j].observations[i].space;
        }
        Mean+=s/nombre_train;
        s=0;
    }
    Mean=Mean/iter;
    cout<<"Mean="<<Mean<<endl;
    for (int i(0);i<iter;i++){
        for(int j(0);j<nombre_train;j++){
            experiment[j].observations[i].space=(experiment[j].observations[i].space-Mean);
            s+=carre(experiment[j].observations[i].space);
        }
        Var+=s/nombre_train;
        s=0;
        
    }
    Var=Var/iter;
    cout<<"Var="<<Var<<endl;
    for (int i(0);i<iter;i++){
        for(int j(0);j<nombre_train;j++){
            experiment[j].observations[i].space=experiment[j].observations[i].space/sqrt(Var);
        }
    }
    */
    return(experiment);
    
}

vector<quantities> autocorrelation_function(vector<FCS_train> simus_normalised){
    vector<quantities> G;
    long iter=simus_normalised[0].observations.size();
    long nombre_simus=simus_normalised.size();
    double step=simus_normalised[0].observations[1].time-simus_normalised[0].observations[0].time;
    double S=0;
    double s=0;
    
    for(double tau(0);tau<iter;tau++){
        S=0;
        for(int i(0);i<nombre_simus;i++){
            s=0;
            for( int j(0);j<iter-tau;j++){
                s+=simus_normalised[i].observations[j].space*simus_normalised[i].observations[j+tau].space;
            }
            S+=s*1.0/((iter-tau));
        }
        G.push_back(quantities(S*1.0/nombre_simus,tau*step));
    }
    return(G);
}


FCS_train bin_FCS_train(vector<double> arrival,double timestep,double T_max){//donner un train fcs avec les temps init aux instant ou on veut binner en 0
    long i_n=1;//indice dans le train binné
    long j_arrival=0;//indiuce dans le train init
    FCS_train n=initialise_FCS_empty(floor(T_max/timestep),timestep);
    
    while(j_arrival<arrival.size()and i_n<n.observations.size()){
        while(arrival[j_arrival]<n.observations[i_n].time and j_arrival<arrival.size()){
            n.observations[i_n].space++;
            j_arrival++;
        }
        i_n++;
    }
    return(n);
}
