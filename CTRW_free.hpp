//
//  CTRW_free.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 08/06/2021.
//

#ifndef CTRW_free_hpp
//#define CTRW_free_hpp

#include "Biblio_generateur_alea.hpp"
#include <stdio.h>

void sim_free_ctrw_d(double alpha, double mu,double sigma, long nombre_simu, vector<vector<vector<double>>>& instants);

#endif /* CTRW_free_hpp */
