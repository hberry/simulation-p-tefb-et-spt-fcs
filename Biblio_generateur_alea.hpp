//
//  Biblio_generateur_alea.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 26/05/2021.
//

#ifndef Biblio_generateur_alea_hpp
#define Biblio_generateur_alea_hpp


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include<queue>
#include<iterator>
#include<cstdlib>
#include<cmath>
#include<time.h>
#include <iostream>
#define PI 3.141592;
const int dimension=3;
 
using namespace std;


double unif();
void polaire(vector<double>& tab);//rajoute 2 normale(0,1) indep a la fin d'un vecteur
void n_polaire(vector<double>& tab, long n, double mu, double sigma);// créé un vecteur de n N(mu, theta^2) iid
void n_polaire(queue<double>& tab, long n);
void n_polaire(vector<double>& tab, long n);
double heavy_tail(double alpha, double x_star);
void n_heavy_tail(vector<double>& va, double alpha, long n);
double heavy_tail(double alpha);
void plus_polaire_3(double tab[3]);
void plus_polaire_3(double tab[3], double sigma[3]);
double expo(double lambda);
double expo_non_homogene(double t,double alpha);
void unif_boule(double X[3],double rayon);
void unif_ellipsoid(double X[3],double sigma[3],double rayon);
double poisson_law(double lambda,double T_max);
int unif(vector<double> poids);

#endif /* Biblio_generateur_alea_hpp */
