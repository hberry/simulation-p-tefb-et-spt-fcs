//
//  CTRW_free.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 08/06/2021.
//

#include "CTRW_free.hpp"

using namespace std;

void sim_free_ctrw_d(double alpha, double mu,double sigma, long nombre_simu, vector<vector<vector<double>>>& instants){//instants contient aussi x0 en plus des instants d'observation colonne 1 les tps d'obs(vecteur de dimension d) et les autres colonnes les ctrw aux temps d'obs
    long dim=instants[0][0].size();
    vector<double> jumps(instants[0].size()-1,instants[0][0][0]);//stock les derniers temps de sauts
    vector<double>normal;
    long r=heavy_tail(alpha/*,x_star*/);
    
    for(int i(0);i<jumps.size();i++){
        jumps[i]=jumps[i]+heavy_tail(alpha);
    }
    
    for(int i(0);i<instants.size()-1;i++){
        for(int j(0);j<instants[0].size()-1;j++){
            r=0;
            while(jumps[j]<instants[i+1][0][0]){//si le temps d'obs est sup au temps de saut
                r=r+1;
                jumps[j]=jumps[j]+heavy_tail(alpha);
            }
            n_polaire(normal,dim*r,mu, sigma);//on fait notre saut
            for(int k(0);k<dim;k++){
                for(int l(0);l<r;l++){
                    instants[i+1][j+1][k]=instants[i][j+1][k]+normal[k*r+l];
                }
                    
            }
            if(r==0){
                instants[i+1][j+1]=instants[i][j+1];
            }
        }
        
    }
    
}
