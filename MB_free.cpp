//
//  MB_free.cpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 26/05/2021.
//

#include "MB_free.hpp"

using namespace std;

void sim_free_mb_d1(vector<vector<double>>& traj, double t_min, double t_max, double b_0, double D, int iter){  // simule un brownien tq B_t_min=b_0 jusqu'à t_max en iter pas. colone 0 les instants colonnes 1 les valeur du brownien
    double h=((t_max-t_min)/iter);
    vector<double> N;
    n_polaire(N, iter,0, 1);
    
    traj.clear();
    vector<double>tab ={t_min,b_0};
    traj.push_back(tab);
    for(int i(1);i<=iter;i++){
        t_min=t_min+h;
        b_0=b_0+sqrt(D*h)*N[i-1];
        vector<double>tab ={t_min,b_0};
        traj.push_back(tab);
    }
    
}

void sim_n_free_mb_d1(double t_min, double t_max, vector<vector<double>>& b_0, double D, int iter){  // simule des brownien tq B_t_min=b_0 (vecteur) jusqu'à t_max en iter pas. colone 0 les instants colonnes 1 les valeur du brownien
    
    double h=(t_max-t_min)/iter;//le pas
    vector<double> N;//nos simulation de loi normale
    long l=b_0[0].size(); //le nombre de simu qu'on veut
    n_polaire(N, iter*l,0, 1);
    
    vector<double>tab (l+1,t_min);
        
    for(int i=1;i<=l;i++){ //j'initialise t0 x0
        tab[i]=b_0[0][i-1];
    }
    
    b_0.clear();
    b_0.push_back(tab);
    
    for(int i(1);i<=iter;i++){
        t_min=t_min+h;
        tab[0]=t_min;
        for(int j=1;j<=l;j++){
            tab[j]=tab[j]+sqrt(D*h)*N[(i-1)*l+j-1];
        }
        
        b_0.push_back(tab);
    }
    
}


void sim_n_free_mb_d(double t_min, double t_max, vector<vector<vector<double>>>& b_0, vector<double> D, long iter){  // simule des brownien tq B_t_min=b_0(vecteur de vecteur de dim d)  jusqu'à t_max en iter pas. colone 0 les instants colonnes i>0 les valeur du brownien
    
    long dimension=b_0[0][0].size();
    double h=(t_max-t_min)/iter;//le pas
    vector<double> N;//nos simulation de loi normale
    long l=b_0[0].size(); //le nombre de simu qu'on veut
    n_polaire(N, iter*l*dimension,0, 1);
    
    vector<vector<double>>tab (l+1,vector<double> (dimension,t_min));//je déclare mon vecteur de construction
        
    for(int i=1;i<=l;i++){ //j'initialise t0 x0
        tab[i]=b_0[0][i-1]; //j'initialise mon vecteur de construction
    }
    
    b_0.clear();
    b_0.push_back(tab);
    
    for(int i(1);i<=iter;i++){
        t_min=t_min+h;
        tab[0]=vector<double>(dimension,t_min);
        for(int j=1;j<=l;j++){
            for(int k=0;k<dimension;k++){
                
                tab[j][k]=tab[j][k]+D[k]*sqrt(h)*N[(i-1)*l*k+j-1+k];
            
            }
        }
        
        b_0.push_back(tab);
    }
    
}
