//
//  simu_objet.hpp
//  Simulation_training
//
//  Created by Nathan Quiblier on 18/06/2021.
//

#ifndef simu_objet_hpp
#define simu_objet_hpp
#include"Biblio_generateur_alea.hpp"
#include <stdio.h>
#include"tools.hpp"

//const int dimension=3;

class particule{
public:
    
    double time;
    double space[dimension];
    
    particule(double x[dimension],double t){
        
        for(int i(0);i<dimension;i++){
            space[i]=x[i];
        }
        time=t;
        }
    
    
};


class trajectoire{
public:
    queue<particule>observations;
    
    double D[dimension];
    trajectoire( double d[dimension],particule b_0){
        
        for(int i(0);i<dimension;i++){
            D[i]=d[i];
        }
        observations.push(b_0);
    }
    
    trajectoire( double d[dimension],queue<particule>data){
       
        for(int i(0);i<dimension;i++){
            D[i]=d[i];
        }
        observations=data;
    }
    
    ~trajectoire(){
        observations={};
        
    }
    
};


class quantities{
public:
    
    double time;
    double space;
    
    quantities(double x,double t){
        
        space=x;
        time=t;
        }
    
    
};
class FCS_photons{
public:
    queue<quantities>observations;
    
    
    FCS_photons( quantities b_0){
        
        
        observations.push(b_0);
    }
    
    FCS_photons( queue<quantities>data){
       
        
        observations=data;
    }
    
    ~FCS_photons(){
        observations={};
    }
    
};

struct point {
    double x;
    double y;
    double z;
};

trajectoire o_sim_free_mb_d(double D[3],particule x0,long iter, double pas);
trajectoire o_sim_free_ctrw_puis(double alpha, queue<double> temps_obs, particule x0);
trajectoire o_sim_free_ctrw_pois(double alpha,queue<double> temps_obs,particule x0);
void msd_MC(vector<trajectoire>& data, queue<quantities>& result);
void msd_traj(vector<trajectoire> data, queue<quantities>& result,double pas);
trajectoire o_sim_free_mbf(vector<vector<double>>& test, particule x0,long iter, double pas, double H);
void regression_alpha(queue<quantities> instant,double alphabeta[2]);
void auto_correlation_de_traj(vector<trajectoire>& data, queue<quantities>& result,double pas);
void auto_correlation(vector<FCS_photons>& data, queue<quantities>& result,double pas);
#endif /* simu_objet_hpp */
